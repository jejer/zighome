#ifndef _ENC28J60_IF_H
#define _ENC28J60_IF_H
#include "lwip/err.h"
#include "lwip/inet.h"
#include "lwip/netif.h"
#include "lwip/pbuf.h"

/* Forward declarations. */
err_t enc28j60_if_init(struct netif *netif);
void enc28j60_if_new_data(struct netif *netif);

#endif /* _ENC28J60_IF_H */
