@echo off
pushd .
cd %~dp0
echo CMD PATH: %cd%
rmdir /s /q Output
cd Project
del /s *.txt
del /s *.log
del /s *.dep
del /s *.bak
del /s *.uvopt
del /s *.uvgui.*
popd
