#ifndef __BASIC_BOARD_H
#define __BASIC_BOARD_H

extern void board_init(void);

/* LED */
// LED1   -- PA4 [NOT USED, conflict with ENC28J60 SPI] ACTIVE_HIGH
// LED2   -- PA1 [Blink while running] ACTIVE_HIGH
extern void LED_init(void);
extern void LED_on(void);
extern void LED_off(void);

/* ENC28J60 -- USE SPI1 */
// RST    -- [NOT USED]
// INT    -- PB0
// CS     -- PA4 SPI1_NSS
// SCK    -- PA5 SPI1_SCK
// SO     -- PA6 SPI1_MOSO
// SI     -- PA7 SPI1_MOSI
extern void enc28j60_io_init(void);

/* USART */
extern void USART_OUT(USART_TypeDef *USARTx, uint8_t *Data, uint16_t Len);
/* USART1 -- [OnBoard TTL2USB] */
// TX     -- PA9
// RX     -- PA10
extern void uart1_init(unsigned int baud_rate);
/* USART2 -- RS232 [NOT USED]*/
// TX     -- PA2
// RX     -- PA3
extern void uart2_init(unsigned int baud_rate);
/* USART3 -- CC2530 [FULL_REMAP]*/
// TX     -- PD8
// RX     -- PD9
extern void uart3_init(unsigned int baud_rate);

#endif
