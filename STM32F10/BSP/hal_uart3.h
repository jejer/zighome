#ifndef HAL_UART3_H
#define HAL_UART3_H

#include <stm32f10x.h>

typedef void (*halUART3CBack_t) (void);

extern void hal_uart3_init(halUART3CBack_t uart3CB);
extern uint16_t hal_uart3_rx_len(void);
extern uint16_t hal_uart3_read(uint8_t *buf, uint16_t len);
extern uint16_t hal_uart3_write(uint8_t *buf, uint16_t len);
extern void hal_uart3_isr(void);

#endif /* HAL_UART1_H */
