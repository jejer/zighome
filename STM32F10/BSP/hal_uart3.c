
/*********************************************************************
 * INCLUDES
 */

#include "hal_uart3.h"
#include "app_cfg.h"
#include "ucos_ii.h"

/*********************************************************************
 * MACROS
 */

#define HAL_UART3_RX_BUF_AVAIL() \
  (uart3Cfg.rxTail >= uart3Cfg.rxHead) ? \
  (uart3Cfg.rxTail - uart3Cfg.rxHead) : \
  (HAL_UART3_RX_MAX - uart3Cfg.rxHead + uart3Cfg.rxTail)

#define HAL_UART3_TX_BUF_AVAIL() \
  (uart3Cfg.txHead > uart3Cfg.txTail) ? \
  (uart3Cfg.txHead - uart3Cfg.txTail - 1) : \
  (HAL_UART3_TX_MAX - uart3Cfg.txTail + uart3Cfg.txHead - 1)

/*********************************************************************
 * CONSTANTS
 */

#define HAL_UART3_RX_MAX 255
#define HAL_UART3_TX_MAX 255

/*********************************************************************
 * TYPEDEFS
 */

typedef struct
{
  uint8_t rxBuf[HAL_UART3_RX_MAX];
  uint16_t rxHead;
  volatile uint16_t rxTail;

  uint8_t txBuf[HAL_UART3_TX_MAX];
  volatile uint16_t txHead;
  uint16_t txTail;

  halUART3CBack_t uart3CB;
} uartCfg_t;

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * GLOBAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static uartCfg_t uart3Cfg;
static OS_STK app_usart3_task_stk[APP_USART3_TASK_STK_SIZE];

/*********************************************************************
 * LOCAL FUNCTIONS
 */

static void usart3_test_echo_cb(void);
static void usart_rx_task(void *p_arg);


void hal_uart3_init(halUART3CBack_t uart3CB)
{ 
  /* ATTACH CALLBACK FUNC */
  if (uart3CB) {
    uart3Cfg.uart3CB = uart3CB;
  }
  else {
    uart3Cfg.uart3CB = usart3_test_echo_cb;
  }
	
	/* CREATE RX THREAD */
	OSTaskCreate(usart_rx_task, (void *)0, 
	             &app_usart3_task_stk[APP_USART3_TASK_STK_SIZE-1], 
	             APP_USART3_TASK_PRIO);

}

uint16_t hal_uart3_rx_len(void)
{
  return HAL_UART3_RX_BUF_AVAIL();
}

uint16_t hal_uart3_read(uint8_t *buf, uint16_t len)
{
  uint16_t cnt = 0;

  while ((uart3Cfg.rxHead != uart3Cfg.rxTail) && (cnt < len))
  {
    *buf++ = uart3Cfg.rxBuf[uart3Cfg.rxHead++];
    if (uart3Cfg.rxHead >= HAL_UART3_RX_MAX)
    {
      uart3Cfg.rxHead = 0;
    }
    cnt++;
  }

  return cnt;
}

uint16_t hal_uart3_write(uint8_t *buf, uint16_t len)
{
  uint16_t cnt;

  // Enforce all or none.
  if (HAL_UART3_TX_BUF_AVAIL() < len)
  {
    return 0;
  }

  for (cnt = 0; cnt < len; cnt++)
  {
    uart3Cfg.txBuf[uart3Cfg.txTail] = *buf++;

    if (uart3Cfg.txTail >= HAL_UART3_TX_MAX-1)
    {
      uart3Cfg.txTail = 0;
    }
    else
    {
      uart3Cfg.txTail++;
    }
		
		USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
  }
	
  return cnt;
}

void hal_uart3_isr(void)
{
	uint8_t temp_rx_data;
	
	/*	
	* TXE: Transmit data register empty
  * This bit is set by hardware when the content of the TDR register has been transferred into
  * the shift register. An interrupt is generated if the TXEIE bit =1 in the USART_CR1 register. It
  * is cleared by a write to the USART_DR register.
  */
	if (USART_GetITStatus(USART3, USART_IT_TXE) == SET) {
		if (uart3Cfg.txHead == uart3Cfg.txTail)
		{
			USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
		}
		else
		{
			USART_SendData(USART3, uart3Cfg.txBuf[uart3Cfg.txHead++]);
			if (uart3Cfg.txHead >= HAL_UART3_TX_MAX)
			{
				uart3Cfg.txHead = 0;
			}
		}		
	}
	/*	
	* RXNE: Read data register not empty
  * This bit is set by hardware when the content of the RDR shift register has been transferred to
  * the USART_DR register. An interrupt is generated if RXNEIE=1 in the USART_CR1 register.
  * It is cleared by a read to the USART_DR register. The RXNE flag can also be cleared by
	* writing a zero to it. This clearing sequence is recommended only for multibuffer
	* communication.
	*/
	if (USART_GetITStatus(USART3, USART_IT_RXNE) == SET) {
		temp_rx_data = USART_ReceiveData(USART3);
		
		uart3Cfg.rxBuf[uart3Cfg.rxTail] = temp_rx_data;	
		if (++uart3Cfg.rxTail >= HAL_UART3_RX_MAX)
		{
			uart3Cfg.rxTail = 0;
		}
	}
	
	/* 
	* CTS: CTS flag
  * This bit is set by hardware when the nCTS input toggles, if the CTSE bit is set. It is cleared
  * by software (by writing it to 0). An interrupt is generated if CTSIE=1 in the USART_CR3
  * register.
  */
	if (USART_GetITStatus(USART3, USART_IT_CTS) == SET) {
		USART_ITConfig(USART3, USART_IT_CTS, DISABLE);
		USART_ClearITPendingBit(USART3, USART_IT_CTS);
	}
	/*	
	* LBD: LIN break detection flag
  * This bit is set by hardware when the LIN break is detected. It is cleared by software (by
  * writing it to 0). An interrupt is generated if LBDIE = 1 in the USART_CR2 register.
  */	
	if (USART_GetITStatus(USART3, USART_IT_LBD) == SET) {
		USART_ITConfig(USART3, USART_IT_LBD, DISABLE);
		USART_ClearITPendingBit(USART3, USART_IT_LBD);
	}
	/*	
	* TC: Transmission complete
  * This bit is set by hardware if the transmission of a frame containing data is complete and if
  * TXE is set. An interrupt is generated if TCIE=1 in the USART_CR1 register. It is cleared by a
  * software sequence (a read from the USART_SR register followed by a write to the
	* USART_DR register). The TC bit can also be cleared by writing a '0' to it. This clearing
	* sequence is recommended only for multibuffer communication.	
	*/
	if (USART_GetITStatus(USART3, USART_IT_TC) == SET) {
		USART_ITConfig(USART3, USART_IT_TC, DISABLE);
		USART_ClearITPendingBit(USART3, USART_IT_TC);
	}
	/*	
	* IDLE: IDLE line detected
  * This bit is set by hardware when an Idle Line is detected. An interrupt is generated if the
  * IDLEIE=1 in the USART_CR1 register. It is cleared by a software sequence (an read to the
  * USART_SR register followed by a read to the USART_DR register).	
  */	
	if (USART_GetITStatus(USART3, USART_IT_IDLE) == SET) {
		USART_ITConfig(USART3, USART_IT_IDLE, DISABLE);
	}
	/*
	 * ORE: Overrun error
	 * This bit is set by hardware when the word currently being received in the shift register is
   * ready to be transferred into the RDR register while RXNE=1. An interrupt is generated if
   * RXNEIE=1 in the USART_CR1 register. It is cleared by a software sequence (an read to the
   * USART_SR register followed by a read to the USART_DR register).
	*/
	if (USART_GetITStatus(USART3, USART_IT_ORE) == SET) {
		USART_ITConfig(USART3, USART_IT_ORE, DISABLE);
	}
	/*
	 * NE: Noise error flag
	 * This bit is set by hardware when noise is detected on a received frame. It is cleared by a
	 * software sequence (an read to the USART_SR register followed by a read to the
	 * USART_DR register).
	*/
	if (USART_GetITStatus(USART3, USART_IT_NE) == SET) {
		USART_ITConfig(USART3, USART_IT_NE, DISABLE);
	}
	/*	
	 * FE: Framing error
	 * This bit is set by hardware when a de-synchronization, excessive noise or a break character
	 * is detected. It is cleared by a software sequence (an read to the USART_SR register
	 * followed by a read to the USART_DR register).	
	*/ 	
	if (USART_GetITStatus(USART3, USART_IT_FE) == SET) {
		USART_ITConfig(USART3, USART_IT_FE, DISABLE);
	}
	/*		
	 * PE: Parity error
	 * This bit is set by hardware when a parity error occurs in receiver mode. It is cleared by a
	 * software sequence (a read to the status register followed by a read to the USART_DR data
	 * register). The software must wait for the RXNE flag to be set before clearing	
	*/ 	
	if (USART_GetITStatus(USART3, USART_IT_PE) == SET) {
		USART_ITConfig(USART3, USART_IT_PE, DISABLE);
	}

}

static void usart3_test_echo_cb(void)
{
  uint8_t temp_echo[10] = {0};
  uint8_t cnt = hal_uart3_read(temp_echo, 10);
  if (cnt > 0) {
    hal_uart3_write(temp_echo, cnt);
  }
}

static void usart_rx_task(void *p_arg)
{
	uint16_t rx_to_read;
	(void)p_arg;
	
  /* Infinite loop */
  while (1)
  {
		OSTimeDlyHMSM(0, 0, 0, 500);
		
		rx_to_read = HAL_UART3_RX_BUF_AVAIL();
		
		if (!uart3Cfg.uart3CB || rx_to_read == 0) {
			continue;
		}
		
		uart3Cfg.uart3CB();
  }	
}
