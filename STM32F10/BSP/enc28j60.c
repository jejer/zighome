/*
 * File      : enc28j60.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2009, RT-Thread Development Team
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rt-thread.org/license/LICENSE
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-05-05     Bernard      the first version
 */
#include "enc28j60.h"

#include <stm32f10x.h>
#include "basic_board.h"
#include <os_cpu.h>
#include <stdio.h>
#include <string.h>
#include "enc28j60_if.h"

#define ENC28J60_CSL() 	GPIOA->BRR = GPIO_Pin_4;
#define ENC28J60_CSH()	GPIOA->BSRR = GPIO_Pin_4;

static uint8_t  Enc28j60Bank;
static uint16_t NextPacketPtr;

void _delay_us(uint32_t us)
{
	uint32_t len;
	for (;us > 0; us --)
		for (len = 0; len < 20; len++ );
}

void delay_ms(uint32_t ms)
{
	uint32_t len;
	for (;ms > 0; ms --)
		for (len = 0; len < 100; len++ );
}

uint8_t spi_read_op(uint8_t op, uint8_t address)
{
	int temp=0;
	ENC28J60_CSL();

	SPI_I2S_SendData(SPI1, (op | (address & ADDR_MASK)));
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);
	SPI_I2S_ReceiveData(SPI1);
	SPI_I2S_SendData(SPI1, 0x00);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);

	// do dummy read if needed (for mac and mii, see datasheet page 29)
	if (address & 0x80)
	{
		SPI_I2S_ReceiveData(SPI1);
		SPI_I2S_SendData(SPI1, 0x00);
		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);
	}
	// release CS

	temp=SPI_I2S_ReceiveData(SPI1);
	// for(t=0;t<20;t++);
	ENC28J60_CSH();
	return (temp);
}

void spi_write_op(uint8_t op, uint8_t address, uint8_t data)
{
	OS_CPU_SR  cpu_sr;

	OS_ENTER_CRITICAL();

	ENC28J60_CSL();

	SPI_I2S_SendData(SPI1, op | (address & ADDR_MASK));
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);
	SPI_I2S_SendData(SPI1,data);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);
	ENC28J60_CSH();

	OS_EXIT_CRITICAL();
}

void enc28j60_set_bank(uint8_t address)
{
	// set the bank (if needed)
	if ((address & BANK_MASK) != Enc28j60Bank)
	{
		// set the bank
		spi_write_op(ENC28J60_BIT_FIELD_CLR, ECON1, (ECON1_BSEL1|ECON1_BSEL0));
		spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, (address & BANK_MASK)>>5);
		Enc28j60Bank = (address & BANK_MASK);
	}
}

uint8_t spi_read(uint8_t address)
{
	// set the bank
	enc28j60_set_bank(address);
	// do the read
	return spi_read_op(ENC28J60_READ_CTRL_REG, address);
}

void spi_read_buffer(uint8_t* data, uint32_t len)
{
	ENC28J60_CSL();

	SPI_I2S_SendData(SPI1,ENC28J60_READ_BUF_MEM);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);

	SPI_I2S_ReceiveData(SPI1);

	while (len)
	{
		len--;
		SPI_I2S_SendData(SPI1,0x00)	;
		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);

		*data= SPI_I2S_ReceiveData(SPI1);
		data++;
	}

	ENC28J60_CSH();
}

void spi_write(uint8_t address, uint8_t data)
{
	// set the bank
	enc28j60_set_bank(address);
	// do the write
	spi_write_op(ENC28J60_WRITE_CTRL_REG, address, data);
}

void enc28j60_phy_write(uint8_t address, uint16_t data)
{
	// set the PHY register address
	spi_write(MIREGADR, address);

	// write the PHY data
	spi_write(MIWRL, data);
	spi_write(MIWRH, data>>8);

	// wait until the PHY write completes
	while (spi_read(MISTAT) & MISTAT_BUSY)
	{
		_delay_us(15);
	}
}

// read upper 8 bits
uint16_t enc28j60_phy_read(uint8_t address)
{
	// Set the right address and start the register read operation
	spi_write(MIREGADR, address);
	spi_write(MICMD, MICMD_MIIRD);

	_delay_us(15);

	// wait until the PHY read completes
	while (spi_read(MISTAT) & MISTAT_BUSY);

	// reset reading bit
	spi_write(MICMD, 0x00);

	return (spi_read(MIRDH));
}

void enc28j60_clkout(uint8_t clk)
{
	//setup clkout: 2 is 12.5MHz:
	spi_write(ECOCON, clk & 0x7);
}

static __inline void enc28j60_interrupt_disable()
{
	/* switch to bank 0 */
	enc28j60_set_bank(EIE);

	/* disable interrutps */
	spi_write_op(ENC28J60_BIT_FIELD_CLR, EIE, EIE_INTIE);
}

static __inline void enc28j60_interrupt_enable()
{
	/* switch to bank 0 */
	enc28j60_set_bank(EIE);

	/* disable interrutps */
	spi_write_op(ENC28J60_BIT_FIELD_SET, EIE, EIE_INTIE);
}

/*
 * Access the PHY to determine link status
 */
static uint8_t enc28j60_check_link_status()
{
	return (enc28j60_phy_read(PHSTAT2) & PHSTAT2_LSTAT)?1:0;
}

void dump_enc28j60(void)
{
	printf("-- enc28j60 registers:\n");
	printf("RevID: 0x%x\n", spi_read(EREVID));

	printf("Cntrl: ECON1 ECON2 ESTAT EIR   EIE\n");
	printf("       0x%02x  0x%02x  0x%02x  0x%02x  0x%02x\n",
		   spi_read(ECON1),
		   spi_read(ECON2),
		   spi_read(ESTAT),
		   spi_read(EIR),
		   spi_read(EIE));

	printf("PHY  : PHCON1  PHSTAT1 PHSTAT2 PHID1   PHID2   PHCON2  PHIE    PHIR    PHLCON\n");
	printf("       0x%04x  0x%04x  0x%04x  0x%04x  0x%04x  0x%04x  0x%04x  0x%04x  0x%04x\n",
		   enc28j60_phy_read(PHCON1),
		   enc28j60_phy_read(PHSTAT1),
		   enc28j60_phy_read(PHSTAT2),
		   enc28j60_phy_read(PHHID1),
		   enc28j60_phy_read(PHHID2),
		   enc28j60_phy_read(PHCON2),
		   enc28j60_phy_read(PHIE),
		   enc28j60_phy_read(PHIR),
		   enc28j60_phy_read(PHLCON));

	printf("MAC  : MACON1 MACON3 MACON4 MAC-Address\n");
	printf("       0x%02x   0x%02x   0x%02x   %02x:%02x:%02x:%02x:%02x:%02x\n",
		   spi_read(MACON1),
		   spi_read(MACON3),
		   spi_read(MACON4),
		   spi_read(MAADR5),
		   spi_read(MAADR4),
		   spi_read(MAADR3),
		   spi_read(MAADR2),
		   spi_read(MAADR1),
		   spi_read(MAADR0));

	printf("Rx   : ERXST  ERXND  ERXWRPT ERXRDPT ERXFCON EPKTCNT MAMXFL\n");
	printf("       0x%04x 0x%04x 0x%04x  0x%04x  0x%02x    0x%02x    0x%04x\n",
		   (spi_read(ERXSTH) << 8) | spi_read(ERXSTL),
		   (spi_read(ERXNDH) << 8) | spi_read(ERXNDL),
		   (spi_read(ERXWRPTH) << 8) | spi_read(ERXWRPTL),
		   (spi_read(ERXRDPTH) << 8) | spi_read(ERXRDPTL),
		   spi_read(ERXFCON),
		   spi_read(EPKTCNT),
		   (spi_read(MAMXFLH) << 8) | spi_read(MAMXFLL));

	printf("Tx   : ETXST  ETXND  MACLCON1 MACLCON2 MAPHSUP\n");
	printf("       0x%04x 0x%04x 0x%02x     0x%02x     0x%02x\n",
		   (spi_read(ETXSTH) << 8) | spi_read(ETXSTL),
		   (spi_read(ETXNDH) << 8) | spi_read(ETXNDL),
		   spi_read(MACLCON1), spi_read(MACLCON2), spi_read(MAPHSUP));
}

/* initialize the interface */
void enc28j60_init(void)
{
	// perform system reset
	spi_write_op(ENC28J60_SOFT_RESET, 0, ENC28J60_SOFT_RESET);
	delay_ms(50);
	NextPacketPtr = RXSTART_INIT;

	// Rx start
	spi_write(ERXSTL, RXSTART_INIT&0xFF);
	spi_write(ERXSTH, RXSTART_INIT>>8);
	// set receive pointer address
	spi_write(ERXRDPTL, RXSTOP_INIT&0xFF);
	spi_write(ERXRDPTH, RXSTOP_INIT>>8);
	// RX end
	spi_write(ERXNDL, RXSTOP_INIT&0xFF);
	spi_write(ERXNDH, RXSTOP_INIT>>8);

	// TX start
	spi_write(ETXSTL, TXSTART_INIT&0xFF);
	spi_write(ETXSTH, TXSTART_INIT>>8);
	// set transmission pointer address
	spi_write(EWRPTL, TXSTART_INIT&0xFF);
	spi_write(EWRPTH, TXSTART_INIT>>8);
	// TX end
	spi_write(ETXNDL, TXSTOP_INIT&0xFF);
	spi_write(ETXNDH, TXSTOP_INIT>>8);

	// do bank 1 stuff, packet filter:
	// For broadcast packets we allow only ARP packtets
	// All other packets should be unicast only for our mac (MAADR)
	//
	// The pattern to match on is therefore
	// Type     ETH.DST
	// ARP      BROADCAST
	// 06 08 -- ff ff ff ff ff ff -> ip checksum for theses bytes=f7f9
	// in binary these poitions are:11 0000 0011 1111
	// This is hex 303F->EPMM0=0x3f,EPMM1=0x30
	spi_write(ERXFCON, ERXFCON_UCEN|ERXFCON_CRCEN|ERXFCON_BCEN);

	// do bank 2 stuff
	// enable MAC receive
	spi_write(MACON1, MACON1_MARXEN|MACON1_TXPAUS|MACON1_RXPAUS);
	// enable automatic padding to 60bytes and CRC operations
	// spi_write_op(ENC28J60_BIT_FIELD_SET, MACON3, MACON3_PADCFG0|MACON3_TXCRCEN|MACON3_FRMLNEN);
	spi_write_op(ENC28J60_BIT_FIELD_SET, MACON3, MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN | MACON3_FULDPX);
	// bring MAC out of reset

	// set inter-frame gap (back-to-back)
	// spi_write(MABBIPG, 0x12);
	spi_write(MABBIPG, 0x15);

	spi_write(MACON4, MACON4_DEFER);
	spi_write(MACLCON2, 63);

	// set inter-frame gap (non-back-to-back)
	spi_write(MAIPGL, 0x12);
	//spi_write(MAIPGH, 0x0C);

	// Set the maximum packet size which the controller will accept
	// Do not send packets longer than MAX_FRAMELEN:
	spi_write(MAMXFLL, MAX_FRAMELEN&0xFF);
	spi_write(MAMXFLH, MAX_FRAMELEN>>8);

	// do bank 3 stuff
	// write MAC address
	// NOTE: MAC address in ENC28J60 is byte-backward
	spi_write(MAADR0, 0x00);
	spi_write(MAADR1, 0x04);
	spi_write(MAADR2, 0xA3);
	spi_write(MAADR3, 0x11);
	spi_write(MAADR4, 0x22);
	spi_write(MAADR5, 0x33);

	/* output off */
	spi_write(ECOCON, 0x00);

	// enc28j60_phy_write(PHCON1, 0x00);
	enc28j60_phy_write(PHCON1, PHCON1_PDPXMD); // full duplex
	// no loopback of transmitted frames
	enc28j60_phy_write(PHCON2, PHCON2_HDLDIS);

	enc28j60_set_bank(ECON2);
	spi_write_op(ENC28J60_BIT_FIELD_SET, ECON2, ECON2_AUTOINC);

	// switch to bank 0
	enc28j60_set_bank(ECON1);
	// enable interrutps
	spi_write_op(ENC28J60_BIT_FIELD_SET, EIE, EIE_INTIE|EIE_PKTIE|EIE_TXIE|EIE_TXERIE|EIE_RXERIE|EIE_LINKIE);
	// enable packet reception
	spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);

	/* clock out */
	enc28j60_clkout(2);

	enc28j60_phy_write(PHLCON, 0xD76);	//0x476
	delay_ms(20);
	dump_enc28j60();
}

void enc28j60_tx_begin(uint16_t len)
{	
	// Wait until isr complete
	while ((spi_read(EIE) & EIE_INTIE) == 0);
	enc28j60_interrupt_disable();
	
	// Wait until last tx complete
	while (spi_read(ECON1) & ECON1_TXRTS);
	
	//Errata: Transmit Logic reset
	spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRST);
	spi_write_op(ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRST);
	spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_TXERIF);

	// Set the write pointer to start of transmit buffer area
	spi_write(EWRPTL, TXSTART_INIT&0xFF);
	spi_write(EWRPTH, TXSTART_INIT>>8);
	// Set the TXND pointer to correspond to the packet size given
	spi_write(ETXNDL, (TXSTART_INIT+ len + 1)&0xFF);
	spi_write(ETXNDH, (TXSTART_INIT+ len + 1)>>8);

	// write per-packet control byte
	spi_write_op(ENC28J60_WRITE_BUF_MEM, 0, 0x00);
}

void enc28j60_tx(uint16_t len, unsigned char *data_ptr)
{
	ENC28J60_CSL();
	
	SPI_I2S_SendData(SPI1, ENC28J60_WRITE_BUF_MEM);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);
	
	while (len)
	{
		SPI_I2S_SendData(SPI1, *data_ptr) ;
		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);
		data_ptr++;
		len--;
	}
	
	ENC28J60_CSH();
}

void enc28j60_tx_end(void)
{
	// send the contents of the transmit buffer onto the network
	spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRTS);
	
	enc28j60_interrupt_enable();
}

uint16_t enc28j60_rx_begin(void)
{
	uint16_t rxstat;
	uint16_t len;
	
	// check if a packet has been received and buffered
	if ( spi_read(EPKTCNT) ==0 ) {
		return 0;
	}

	// Set the read pointer to the start of the received packet
	spi_write(ERDPTL, (NextPacketPtr));
	spi_write(ERDPTH, (NextPacketPtr)>>8);
	// read the next packet pointer
	NextPacketPtr  = spi_read_op(ENC28J60_READ_BUF_MEM, 0);
	NextPacketPtr |= spi_read_op(ENC28J60_READ_BUF_MEM, 0)<<8;
	// read the packet length
	len  = spi_read_op(ENC28J60_READ_BUF_MEM, 0);
	len |= spi_read_op(ENC28J60_READ_BUF_MEM, 0)<<8;
	// read the receive status
	rxstat  = spi_read_op(ENC28J60_READ_BUF_MEM, 0);
	rxstat |= spi_read_op(ENC28J60_READ_BUF_MEM, 0)<<8;

	// (we reduce the MAC-reported length by 4 to remove the CRC)
	if (len >= 4) {
		len-=4;
	}

	// Check CRC and symbol errors
	//if ((rxstat & 0x80)==0) {
	//	// invalid
	//	len=0;
	//}

	return len;
}

void enc28j60_rx(uint16_t len, unsigned char *data_ptr)
{
	// copy the packet from the receive buffer
	ENC28J60_CSL();

	SPI_I2S_SendData(SPI1, ENC28J60_READ_BUF_MEM);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);

	SPI_I2S_ReceiveData(SPI1);

	while (len)
	{
		len--;
		SPI_I2S_SendData(SPI1, 0x00);
		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET);

		*data_ptr= SPI_I2S_ReceiveData(SPI1);
		data_ptr++;
	}

	ENC28J60_CSH();
}

void enc28j60_rx_end(void)
{
	// Move the RX read pointer to the start of the next received packet
	// This frees the memory we just read out
	if ( ((NextPacketPtr - 1) < RXSTART_INIT) || ((NextPacketPtr - 1) > RXSTOP_INIT))
	{
		/* Reset the receiver logic */
		enc28j60_set_bank(ECON1);
		spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXRST);
		spi_write_op(ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_RXRST);
		
		NextPacketPtr = RXSTART_INIT;
		// Rx start
		spi_write(ERXSTL, RXSTART_INIT&0xFF);
		spi_write(ERXSTH, RXSTART_INIT>>8);
		// set receive pointer address
		spi_write(ERXRDPTL, RXSTOP_INIT&0xFF);
		spi_write(ERXRDPTH, RXSTOP_INIT>>8);
		// RX end
		spi_write(ERXNDL, RXSTOP_INIT&0xFF);
		spi_write(ERXNDH, RXSTOP_INIT>>8);
		
		enc28j60_set_bank(ESTAT);
		spi_write_op(ENC28J60_BIT_FIELD_CLR, ESTAT, ESTAT_BUFER);
		enc28j60_set_bank(EIR);
		spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_RXERIF);
		
		// enable packet reception
		spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);
	}
	else
	{
		spi_write(ERXRDPTL, (NextPacketPtr));
		spi_write(ERXRDPTH, (NextPacketPtr)>>8);
	}

	// decrement the packet counter indicate we are done with this packet
	spi_write_op(ENC28J60_BIT_FIELD_SET, ECON2, ECON2_PKTDEC);
}

void enc28j60_isr()
{
	// Defined in main.c
	extern struct netif enj28j60_if;
	
	/* Variable definitions can be made now. */
	volatile uint32_t eir, pk_counter;
	volatile uint8_t rx_activiated;
	
	/* Disable all enc28j60 interrupts */
	enc28j60_interrupt_disable();

	/* get EIR */
	eir = spi_read(EIR);
	
	do
	{
		/* errata #4, PKTIF does not reliable */
		pk_counter = spi_read(EPKTCNT);
		if (pk_counter)
		{
			/* a frame has been received */
			enc28j60_if_new_data(&enj28j60_if);
		}

		/* clear PKTIF */
		if (eir & EIR_PKTIF)
		{
			enc28j60_set_bank(EIR);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_PKTIF);
		}

		/* clear DMAIF */
		if (eir & EIR_DMAIF)
		{
			enc28j60_set_bank(EIR);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_DMAIF);
		}

		/* LINK changed handler */
		if ( eir & EIR_LINKIF)
		{
			enc28j60_check_link_status();

			/* read PHIR to clear the flag */
			enc28j60_phy_read(PHIR);

			enc28j60_set_bank(EIR);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_LINKIF);
		}

		if (eir & EIR_TXIF)
		{
			/* A frame has been transmitted. */
			enc28j60_set_bank(EIR);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_TXIF);
		}

		/* TX Error handler */
		if (eir & EIR_TXERIF)
		{
			enc28j60_set_bank(ECON1);
			spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRST);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRST);
			enc28j60_set_bank(EIR);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_TXERIF);
		}

		/* RX Error handler */
		if (eir & EIR_RXERIF)
		{
			/* Reset the receiver logic */
			enc28j60_set_bank(ECON1);
			spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXRST);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_RXRST);
			
			NextPacketPtr = RXSTART_INIT;
			// Rx start
			spi_write(ERXSTL, RXSTART_INIT&0xFF);
			spi_write(ERXSTH, RXSTART_INIT>>8);
			// set receive pointer address
			spi_write(ERXRDPTL, RXSTOP_INIT&0xFF);
			spi_write(ERXRDPTH, RXSTOP_INIT>>8);
			// RX end
			spi_write(ERXNDL, RXSTOP_INIT&0xFF);
			spi_write(ERXNDH, RXSTOP_INIT>>8);
			
			enc28j60_set_bank(ESTAT);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, ESTAT, ESTAT_BUFER);
			enc28j60_set_bank(EIR);
			spi_write_op(ENC28J60_BIT_FIELD_CLR, EIR, EIR_RXERIF);
			
			// enable packet reception
			spi_write_op(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);
		}
		
		eir = spi_read(EIR);
	} while (eir != 0);
	
	/* Enable enc28j60 interrupts */
	enc28j60_interrupt_enable();
}

