/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "ucos_ii.h"
#include "app_cfg.h"
#include "basic_board.h"
#include "enc28j60.h"
#include "lwip\init.h"
#include "lwip\tcpip.h"
#include "lwip\netif.h"
#include "lwip\dhcp.h"
#include "enc28j60_if.h"
#include "hal_uart3.h"
#include "zh_process.h"
#include <stdio.h>
//#include "httpserver-netconn.h"
#include "httpserver_raw\httpd.h"

/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */
	
struct netif enj28j60_if;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static OS_STK app_led_task_stk[APP_CFG_TASK_LED_STK_SIZE];
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static void LED_task(void *p_arg);
static void network_init(void);

	
/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f10x_xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f10x.c file
     */     

  /* Initialize LEDs, Key Button, LCD and COM port(USART) available on
     BASIC board ******************************************************/
	board_init();
	
	printf("------------JEJER\n"
	       "---STM32F103C8---\n"
	       "----ENC28J60-----\n"
	       "---ucos v%d ---\n"
	       "---lwIP v%d.%d.%d---\n", 
	       OS_VERSION,
	       LWIP_VERSION_MAJOR,
	       LWIP_VERSION_MINOR,
	       LWIP_VERSION_REVISION);
	
	OSInit();
	
	network_init();
	zh_process_init();
	
	OSTaskCreate(LED_task, (void *)0, 
	             &app_led_task_stk[APP_CFG_TASK_LED_STK_SIZE-1], APP_CFG_TASK_LED_PRIO);

							 
  OSStart();
	return 0;
}

static void LED_task(void *p_arg)
{
	(void)p_arg;
	
  /* Infinite loop */
  while (1)
  {
		LED_off();
		OSTimeDlyHMSM(0, 0, 1, 0);
		LED_on();
		OSTimeDlyHMSM(0, 0, 1, 0);
		//dump_enc28j60();
  }	
}



static void network_init(void)
{
	struct ip_addr ipaddr;
	struct ip_addr netmask;
	struct ip_addr gw;

#if LWIP_DHCP
	IP4_ADDR(&ipaddr, 0, 0, 0, 0);
	IP4_ADDR(&netmask, 0, 0, 0, 0);
	IP4_ADDR(&gw, 0, 0, 0, 0);
#else
	IP4_ADDR(&ipaddr, 192, 168, 77, 77);
	IP4_ADDR(&netmask, 255, 255, 255, 0);
	IP4_ADDR(&gw, 192, 168, 77, 1);
#endif
	
	tcpip_init(NULL, NULL);
	netif_add(&enj28j60_if, &ipaddr, &netmask, &gw, NULL, &enc28j60_if_init, &tcpip_input);
	netif_set_default(&enj28j60_if);
	
#if LWIP_DHCP
	dhcp_start(&enj28j60_if);
#endif
	netif_set_up(&enj28j60_if);
	
	//http_server_netconn_init();
	httpd_init();
}




/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
