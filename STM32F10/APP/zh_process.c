/*********************************************************************
 * INCLUDES
 */
 
#include "zh_process.h"
#include "ucos_ii.h"
#include "app_cfg.h"
#include "zh_protocol.h"
#include <stdlib.h> // for NULL
#include <string.h> // for memcpy
#include <stdio.h>  // for printf

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * GLOBAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static LIST_HEAD(DEV_LIST);
static OS_STK app_zighome_task_stk[APP_ZIGHOME_TASK_STK_SIZE];

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void ZigHome_task(void *p_arg);
static void zh_process_cb(proto_zh_msg_t *msg);
static void zh_cb_dev_rsp(proto_zh_msg_t *msg);

void zh_process_init(void)
{
	proto_zh_init(zh_process_cb);
	
	OSTaskCreate(ZigHome_task, (void *)0, 
	             &app_zighome_task_stk[APP_ZIGHOME_TASK_STK_SIZE-1], APP_ZIGHOME_TASK_PRIO);
}

static void ZigHome_task(void *p_arg)
{
	zh_device_status_t *p_dev_st = NULL;
	static int counter = 0;
	zh_msg_zl_color_t color_msg;
	(void)p_arg;
	
  /* Infinite loop */
  while (1)
  {
		OSTimeDlyHMSM(0, 0, 1, 0);
		
		proto_zh_send(ZH_COMMON, ZH_COMMON_DEV_LIST_REQ, 0, NULL);
		
		//counter++;
		switch (counter) {
		case 2:
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_OFF, sizeof(zh_dev_t), &(p_dev_st->dev_info));
			}
			break;
		case 4:
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_ON, sizeof(zh_dev_t), &(p_dev_st->dev_info));
			}
			break;
		case 6:
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_TOGGLE, sizeof(zh_dev_t), &(p_dev_st->dev_info));
			}
			break;
		case 8:
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_TOGGLE, sizeof(zh_dev_t), &(p_dev_st->dev_info));
			}
			break;
			
		case 10: /* RED */
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				memcpy(&(color_msg.dev_info), &(p_dev_st->dev_info), sizeof(zh_dev_t));
				color_msg.hue = 0x00;
				color_msg.sat = 0xFF;
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_COLOR, sizeof(zh_msg_zl_color_t), &color_msg);
			}
			break;
		case 12: /* GREEN */
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				memcpy(&(color_msg.dev_info), &(p_dev_st->dev_info), sizeof(zh_dev_t));
				color_msg.hue = 0x55;
				color_msg.sat = 0xFF;
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_COLOR, sizeof(zh_msg_zl_color_t), &color_msg);
			}
			break;
		case 14: /* BLUE */
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				memcpy(&(color_msg.dev_info), &(p_dev_st->dev_info), sizeof(zh_dev_t));
				color_msg.hue = 0xAA;
				color_msg.sat = 0xFF;
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_COLOR, sizeof(zh_msg_zl_color_t), &color_msg);
			}
			break;
		case 16: /* WHITE */
			list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
				memcpy(&(color_msg.dev_info), &(p_dev_st->dev_info), sizeof(zh_dev_t));
				color_msg.hue = 0x00;
				color_msg.sat = 0x00;
				proto_zh_send(ZH_LIGHT, ZH_LIGHT_COLOR, sizeof(zh_msg_zl_color_t), &color_msg);
			}
			break;
			
		case 18:
			counter = 0;
			break;
		default:
			break;
		}

  }	
}

static void zh_process_cb(proto_zh_msg_t *msg)
{	
	switch (msg->cmd1) {
	case ZH_COMMON:
		switch (msg->cmd2) {
		case ZH_COMMON_PRINT:
			break;
		case ZH_COMMON_DEV_LIST_REQ:
			break;
		case ZH_COMMON_DEV_LIST_RSP:
			zh_cb_dev_rsp(msg);
			break;
		default:
			break;
		}
		break;
		
	case ZH_PLUG:
		switch (msg->cmd2) {
		case ZH_PLUG_ON:
			break;
		case ZH_PLUG_OFF:
			break;
		default:
			break;
		}
		break;
		
	case ZH_LIGHT:
		switch (msg->cmd2) {
		case ZH_LIGHT_ON:
			break;
		case ZH_LIGHT_OFF:
			break;
		default:
			break;
		}
		break;
		
	default:
		break;
	}
}

static void zh_cb_dev_rsp(proto_zh_msg_t *msg)
{
	uint8_t i = 0;
	uint8_t exist = 0;
	zh_dev_t *p_msg_dev = NULL;
	zh_device_status_t *p_dev_st = NULL;
	
	uint8_t count = msg->data_len / sizeof(zh_dev_t);
	for (i = 0; i < count; ++i) {
		exist = 0;
		p_msg_dev = (zh_dev_t *)(msg->data + (i * sizeof(zh_dev_t)));
		
		list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
			if (memcmp(&(p_dev_st->dev_info), p_msg_dev->id, ZH_DEVICE_ID_LEN) == 0) {
				exist = 1;
				break;
			}
		}
		
		if (!exist) {
			p_dev_st = NULL;
			p_dev_st = (zh_device_status_t *)malloc(sizeof(zh_device_status_t));
			if (!p_dev_st) {
				printf("zh_cb_dev_rsp malloc fail.\r\n");
				continue;
			}
			memset(p_dev_st, 0, sizeof(zh_device_status_t));
			
			memcpy(&(p_dev_st->dev_info), p_msg_dev->id, ZH_DEVICE_ID_LEN);
			p_dev_st->dev_info.device_type = p_msg_dev->device_type;
			p_dev_st->dev_info.shortAddr[0] = p_msg_dev->shortAddr[0];
			p_dev_st->dev_info.shortAddr[1] = p_msg_dev->shortAddr[1];
			p_dev_st->dev_info.endPoint = p_msg_dev->endPoint;
			
			list_add(&(p_dev_st->head), &DEV_LIST);
		} else {
			p_dev_st->dev_info.shortAddr[0] = p_msg_dev->shortAddr[0];
			p_dev_st->dev_info.shortAddr[1] = p_msg_dev->shortAddr[1];
			p_dev_st->dev_info.device_type = p_msg_dev->device_type;
			p_dev_st->dev_info.endPoint = p_msg_dev->endPoint;
		}
	}
	
	// DEBUG
//	printf("ZH_COMMON_DEV_LIST_RSP\r\n");
//	list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
//		dump_hex((uint8_t *)p_dev_st, sizeof(zh_device_status_t));
//		printf("\r\n");
//	}
//	printf("ZH_COMMON_DEV_LIST_RSP END\r\n");
}


void light_ctrl(int id, int act, int hue, int sat)
{
	zh_msg_zl_color_t color_msg;
	zh_device_status_t *p_dev_st = NULL;
	
	list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
		if (p_dev_st->dev_info.shortAddr[0] == (id >> 8) &&
			  p_dev_st->dev_info.shortAddr[1] == (id & 0xFF)) {
			break;
		}
	}
		
	switch (act) {
	case ZH_LIGHT_ON:
	case ZH_LIGHT_OFF:
		proto_zh_send(ZH_LIGHT, act, sizeof(zh_dev_t), &(p_dev_st->dev_info));
		break;
	case ZH_LIGHT_COLOR:
		memcpy(&(color_msg.dev_info), &(p_dev_st->dev_info), sizeof(zh_dev_t));
		color_msg.hue = hue;
		color_msg.sat = sat;
		proto_zh_send(ZH_LIGHT, ZH_LIGHT_COLOR, sizeof(zh_msg_zl_color_t), &color_msg);
		break;
	default:
		break;
	}
}

uint16_t http_ssi(int iIndex, char *pcInsert, int iInsertLen)
{
	zh_device_status_t *p_dev_st = NULL;
#define TMP_BUF_LEN 64
	char tmp[TMP_BUF_LEN] = {0};
	pcInsert[0] = 0;

	/* SSI Device list */
	if (iIndex == 0) {
		if (list_empty(&DEV_LIST)) {
			sprintf(pcInsert, "<p>No Device!</p>");
			return strlen(pcInsert);
		}
		
		list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
			sprintf(tmp, "<p>MAC: %02X:%02X:%02X:%02X:%02X:%02X:%02X:%02X</p>",
							p_dev_st->dev_info.id[7], p_dev_st->dev_info.id[6], p_dev_st->dev_info.id[5], p_dev_st->dev_info.id[4], 
							p_dev_st->dev_info.id[3], p_dev_st->dev_info.id[2], p_dev_st->dev_info.id[1], p_dev_st->dev_info.id[0]);
			if ((strlen(pcInsert) + strlen(tmp) + 1) > iInsertLen) {
				return strlen(pcInsert);
			}
			strcat(pcInsert, tmp);
			
			sprintf(tmp, "<p>NWK ID: 0x%02X%02X</p><p>Device Type: %d</p>",
							p_dev_st->dev_info.shortAddr[0], p_dev_st->dev_info.shortAddr[1], p_dev_st->dev_info.device_type);
			if ((strlen(pcInsert) + strlen(tmp) + 1) > iInsertLen) {
				return strlen(pcInsert);
			}
			strcat(pcInsert, tmp);
		}
		
		return (strlen(pcInsert) + 1);
	}
	
	/* SSI Device options */
	if (iIndex == 1) {
		if (list_empty(&DEV_LIST)) {
			return 0;
		}
		list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
			sprintf(tmp, "<option value=\"%02X%02X\">0x%02X%02X</option>",
							p_dev_st->dev_info.shortAddr[0], p_dev_st->dev_info.shortAddr[1], 
							p_dev_st->dev_info.shortAddr[0], p_dev_st->dev_info.shortAddr[1]);
			if ((strlen(pcInsert) + strlen(tmp) + 1) > iInsertLen) {
				return strlen(pcInsert);
			}
			strcat(pcInsert, tmp);
		}
		
		return (strlen(pcInsert) + 1);
	}
	
	return 0;
}
