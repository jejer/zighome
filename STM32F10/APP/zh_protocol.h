#ifndef __PROTOCOL_ZIGHOME_H__
#define __PROTOCOL_ZIGHOME_H__

#include <stm32f10x.h>

typedef struct
{
	uint8_t             sof;
  uint8_t             data_len;
  uint8_t             cmd1;
  uint8_t             cmd2;
  uint8_t             data[1];
} proto_zh_msg_t;

typedef void (*proto_zh_cb_t) (proto_zh_msg_t *msg);
extern void proto_zh_init(proto_zh_cb_t cb);
extern uint8_t proto_zh_send(uint8_t cmd1, uint8_t cmd2, uint8_t data_len, void *data);

#endif
