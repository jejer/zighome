/*
lighting control CGI 
*/

#include <string.h>
#include <stdlib.h>
#include "zh_process.h"
#include "lighting_control_cgi.h"

/* Handler of SSI request using create web page */
u16_t lighting_control_SSI(int iIndex, char *pcInsert, int iInsertLen)
{
	return http_ssi(iIndex, pcInsert, iInsertLen);
}

const char *lighting_control_ssis[] = {"DevList", "DevOpt"};

/* Handler of CGI request using turn on or turn off the light */
const char *lighting_control_CGI(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
	int id = 0, act = 0, hue = 0, sat = 0, param;
	
	// Get number and operation
	if(iNumParams > 0) {
		for(param = 0; param < iNumParams; param++) {
			if (strncmp(pcParam[param], "id", 3) == 0) {
				sscanf(pcValue[param], "%x", &id);
				continue;
			}
			if (strncmp(pcParam[param], "act", 4) == 0) {
				if (strncmp(pcValue[param], "on", 3) == 0) {
					act = ZH_LIGHT_ON;
					continue;
				}
				if (strncmp(pcValue[param], "off", 4) == 0) {
					act = ZH_LIGHT_OFF;
					continue;
				}
				if (strncmp(pcValue[param], "color", 6) == 0) {
					act = ZH_LIGHT_COLOR;
					continue;
				}
			}
			if (strncmp(pcParam[param], "hue", 4) == 0) {
				hue = atoi(pcValue[param]);
				continue;
			}
			if (strncmp(pcParam[param], "sat", 4) == 0) {
				sat = atoi(pcValue[param]);
				continue;
			}			
		}
	}
	
	// Turn on or turn off the light
	light_ctrl( id, act, hue, sat );
	
	return "/index.html";
}

tCGI lighting_control_cgis[] = { {"/control.cgi", lighting_control_CGI} };

int lighting_control_cgis_count = 1;

void http_start(void)
{
	http_set_ssi_handler(lighting_control_SSI, lighting_control_ssis, sizeof(lighting_control_ssis)/sizeof(char*));
	http_set_cgi_handlers(lighting_control_cgis, sizeof(lighting_control_cgis)/sizeof(tCGI));
}

/* Buffer for post file */
char post_buffer[64];
int  post_buf_write_pos = 0;

/* Target URI when POST is used */
char *post_target_uri = NULL;
                                
/* POST handlers */
err_t httpd_post_begin(void *connection, const char *uri, const char *http_request,
               u16_t http_request_len, int content_len, char *response_uri,
               u16_t response_uri_len, u8_t *post_auto_wnd)
{
	post_buf_write_pos = 0;
	post_target_uri = (char *)uri;
	return ERR_OK;
}

err_t httpd_post_receive_data(void *connection, struct pbuf *p)
{
	struct pbuf *pnow = p;
	char *data, *start, *end;
	int len;

	/* 
	* If it is a first part we had to replace boundary string 
	* with all headers until CRLFCRLF
	*/
	if(post_buf_write_pos == 0) 
	{
		/* CRLFCRLF must be in first pbuf or in one of the next pbuf */ 
		while(pnow) 
		{
			data = pnow->payload;
			len = pnow->len;

			start = strstr(data, "\r\n\r\n");
			if(start)
			{
				len -= (start - data);
				data = start;
				break;
			}
			pnow = pnow->next;
		}
		/* If there are no CRLFCRLF it is error */
		if(!pnow)
			return ERR_VAL;
	}

	/*
	 * Append posted data to our buffer 
	 * from all pbufs starting from CRLFCRLF 
	 * and until CRLF----
	 * We don't search whole boundary string because it's too long
	 */
	while(pnow) 
	{
		end = strstr(data, "\r\n----");
		if(end)
			len = (end - data);
		
		memcpy(post_buffer + post_buf_write_pos, data, len);
		post_buf_write_pos += len;
		if(end)
			break;
		
		pnow = pnow->next;
		if(pnow) 
		{
			data = pnow->payload;
			len = pnow->len;
		}
	}
	pbuf_free(p);

	return ERR_OK;
}

void httpd_post_finished(void *connection, char *response_uri, u16_t response_uri_len)
{
	post_buffer[post_buf_write_pos] = 0;
	if(post_target_uri) 
	{
		strcpy(response_uri, post_target_uri);
	}
}
