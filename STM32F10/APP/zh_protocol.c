/*********************************************************************
 * INCLUDES
 */

#include "hal_uart3.h"
#include "zh_protocol.h"
#include <stdlib.h>
#include <string.h>  // memcpy
#include <stdio.h>   // printf for debug
#include "ucos_ii.h"
#include "app_cfg.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/* Start-of-frame delimiter for UART transport */
#define PROTO_ZH_UART_SOF                     0xFE

/* state values */
enum proto_zh_state_enum {
  PROTO_ZH_SOF = 0,
  PROTO_ZH_LENTH,
  PROTO_ZH_CMD1,
  PROTO_ZH_CMD2,
  PROTO_ZH_DATA,
  PROTO_ZH_FCS,
};

#define PROTO_ZH_RX_BUF_LEN                   0xFE

/*********************************************************************
 * TYPEDEFS
 */

typedef struct {
  enum proto_zh_state_enum state;
  uint8_t data_len;
  uint8_t data_cur;
  uint8_t fcs;
  proto_zh_cb_t app_cb;
} proto_zh_status_t;

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * GLOBAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static OS_EVENT  *protocol_usart_tx_mutex = NULL;

static proto_zh_status_t proto_zh_status;
static proto_zh_msg_t *p_proto_msg;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void proto_zh_process(void);
static uint8_t zh_calc_fcs(uint8_t *msg_ptr, uint8_t len);
static void proto_zh_test_cb(proto_zh_msg_t *msg);

void proto_zh_init(proto_zh_cb_t cb)
{
	uint8_t err = 0;
  proto_zh_status.state = PROTO_ZH_SOF;
	
	if (cb) {
		proto_zh_status.app_cb = cb;
	}
	else {
		proto_zh_status.app_cb = proto_zh_test_cb;
	}
	
  hal_uart3_init(proto_zh_process);
	
	protocol_usart_tx_mutex = OSMutexCreate(0, &err);
	if (err != OS_ERR_NONE) {
		printf("OSMutexCreate(protocol_usart_tx_mutex) fail %d.\r\n", err);
		protocol_usart_tx_mutex = NULL;
	}
}

/***************************************************************************************************
 * @fn      proto_zh_process(void)
 *
 * @brief   | SOF | Data Length  |   CMD   |   Data   |  FCS  |
 *          |  1  |     1        |    2    |  0-Len   |   1   |
 *
 *          Parses the data then send the data to correct place
 *
 * @return  None
 ***************************************************************************************************/
static void proto_zh_process(void)
{
  uint8_t  data;
  uint8_t  bytes_in_rx_buff;

  while (hal_uart3_rx_len())
  {
    hal_uart3_read(&data, 1);

    switch (proto_zh_status.state)
    {
      case PROTO_ZH_SOF:
        if (data == PROTO_ZH_UART_SOF)
          proto_zh_status.state = PROTO_ZH_LENTH;
        break;

      case PROTO_ZH_LENTH:
        proto_zh_status.data_len = data;
				if (proto_zh_status.data_len > (PROTO_ZH_RX_BUF_LEN-sizeof(proto_zh_msg_t))) {
					printf("ZH_PROTOCOL, RX data_len too big.\r\n");
					
					/* Reset RX state */
					proto_zh_status.state = PROTO_ZH_SOF;
					proto_zh_status.data_len = 0;
					break;
				}

        /* Allocate memory for the data */
        p_proto_msg = (proto_zh_msg_t *)malloc(sizeof ( proto_zh_msg_t ) + proto_zh_status.data_len );

        if (p_proto_msg)
        {
          /* Fill up what we can */
					p_proto_msg->sof = PROTO_ZH_UART_SOF;
          p_proto_msg->data_len = proto_zh_status.data_len;
          proto_zh_status.state = PROTO_ZH_CMD1;
        }
        else
        {
					printf("proto_zh_process malloc fail.\r\n");
          proto_zh_status.state = PROTO_ZH_SOF;
          return;
        }
        break;

      case PROTO_ZH_CMD1:
        p_proto_msg->cmd1 = data;
        proto_zh_status.state = PROTO_ZH_CMD2;
        break;

      case PROTO_ZH_CMD2:
        p_proto_msg->cmd2 = data;
        /* If there is no data, skip to FCS state */
        if (proto_zh_status.data_len)
        {
          proto_zh_status.state = PROTO_ZH_DATA;
          proto_zh_status.data_cur = 0;
        }
        else
        {
          proto_zh_status.state = PROTO_ZH_FCS;
        }
        break;

      case PROTO_ZH_DATA:

        /* Fill in the buffer the first byte of the data */
        p_proto_msg->data[proto_zh_status.data_cur++] = data;

        /* Check number of bytes left in the Rx buffer */
        bytes_in_rx_buff = hal_uart3_rx_len();

        /* If the remain of the data is there, read them all, otherwise, just read enough */
        if (bytes_in_rx_buff <= proto_zh_status.data_len - proto_zh_status.data_cur)
        {
          hal_uart3_read(&p_proto_msg->data[proto_zh_status.data_cur], bytes_in_rx_buff);
          proto_zh_status.data_cur += bytes_in_rx_buff;
        }
        else
        {
          hal_uart3_read(&p_proto_msg->data[proto_zh_status.data_cur], proto_zh_status.data_len - proto_zh_status.data_cur);
          proto_zh_status.data_cur += (proto_zh_status.data_len - proto_zh_status.data_cur);
        }

        /* If number of bytes read is equal to data length, time to move on to FCS */
        if (proto_zh_status.data_len == proto_zh_status.data_cur)
            proto_zh_status.state = PROTO_ZH_FCS;

        break;

      case PROTO_ZH_FCS:

        proto_zh_status.fcs = data;

        /* Make sure it's correct */
        if ((zh_calc_fcs ((uint8_t*)p_proto_msg, sizeof(proto_zh_msg_t)-1 + proto_zh_status.data_len) == proto_zh_status.fcs))
        {
					/* Call upper level procedure */
          proto_zh_status.app_cb(p_proto_msg);
        }
        else
        {
					printf("PROTOCOL_ZIGHOME FCS doesn't match.\r\n");
        }
				
        /* Deallocate the msg */
        free(p_proto_msg);
				p_proto_msg = NULL;
				
        /* Reset the state, send or discard the buffers at this point */
        proto_zh_status.state = PROTO_ZH_SOF;

        break;

      default:
       break;
    }
  }
}

uint8_t proto_zh_send(uint8_t cmd1, uint8_t cmd2, uint8_t data_len, void *data)
{
	uint8_t err = 0;
	proto_zh_msg_t *out_msg = NULL;
	
	OSMutexPend(protocol_usart_tx_mutex, 0, &err);
	if (err != OS_ERR_NONE) {
		printf("OSMutexPend(protocol_usart_tx_mutex) fail %d.\r\n", err);
		return 1;
	}
	
	out_msg = (proto_zh_msg_t *)malloc(sizeof ( proto_zh_msg_t ) + data_len );
	if (!out_msg) {
		printf("proto_zh_send malloc fail.\r\n");
		return 1;
	}	
	
	out_msg->sof = PROTO_ZH_UART_SOF;
	out_msg->cmd1 = cmd1;
	out_msg->cmd2 = cmd2;
	out_msg->data_len = data_len;
	if (data_len > 0) {
		memcpy(out_msg->data, data, data_len);
	}	
	
	out_msg->data[data_len] = zh_calc_fcs((uint8_t *)out_msg, sizeof(proto_zh_msg_t)-1 + data_len);
	
	hal_uart3_write((uint8_t *)out_msg, sizeof(proto_zh_msg_t) + data_len);
	
	free(out_msg);
	
	if (OSMutexPost(protocol_usart_tx_mutex) != OS_ERR_NONE) {
		printf("OSMutexPost(protocol_usart_tx_mutex) fail.\r\n");
		return 1;
	}
	return 0;
}

static uint8_t zh_calc_fcs(uint8_t *msg_ptr, uint8_t len)
{
  uint8_t x;
  uint8_t xorResult;

  xorResult = 0;

  for ( x = 0; x < len; x++, msg_ptr++ )
    xorResult = xorResult ^ *msg_ptr;

  return ( xorResult );
}

static void proto_zh_test_cb(proto_zh_msg_t *msg)
{
	printf("\r\nCMD1(%02x) CMD2(%02x) DATA_LEN(%d) DATA[0](%02x) DATA[1](%02x)\r\n", 
	       msg->cmd1, msg->cmd2, msg->data_len, msg->data[0], msg->data[1]);
}
