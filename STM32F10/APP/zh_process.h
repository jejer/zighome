#ifndef __ZH_PROCESS_H__
#define __ZH_PROCESS_H__

#include <stm32f10x.h>
#include "linux_list.h"

enum zh_cmd1 {
	ZH_COMMON = 0,
	ZH_PLUG,
	ZH_LIGHT,
};

enum zh_cmd2 {
	ZH_COMMON_PRINT = 0,
	ZH_COMMON_DEV_LIST_REQ,
	ZH_COMMON_DEV_LIST_RSP,
	ZH_PLUG_DEV_ST_REQ,
	ZH_PLUG_DEV_ST_RSP,
	ZH_LIGHT_DEV_ST_REQ,
	ZH_LIGHT_DEV_ST_RSP,
	ZH_PLUG_ON,
	ZH_PLUG_OFF,
	ZH_LIGHT_TOGGLE,
	ZH_LIGHT_ON,
	ZH_LIGHT_OFF,
	ZH_LIGHT_COLOR,
};

enum zh_device_type {
	ZH_DEVICE_NULL = 0,
	ZH_DEVICE_PLUG,
	ZH_DEVICE_LIGHT,
};

typedef struct {
	uint8_t is_on;
} zh_plug_status_t;

typedef struct {
	uint8_t is_on;
	uint8_t level;
	uint8_t color[3];
} zh_light_status_t;

#define ZH_DEVICE_ID_LEN 8
typedef struct {
	uint8_t             id[ZH_DEVICE_ID_LEN];
	uint8_t             device_type;
	uint8_t             shortAddr[2];
	uint8_t             endPoint;
} zh_dev_t;

typedef struct {
	struct list_head    head;
	zh_dev_t            dev_info;
	union device_status {
		zh_plug_status_t  zh_plug_status;
		zh_light_status_t zh_light_status;
	} device_status;
} zh_device_status_t;

typedef struct {
	zh_dev_t dev_info;
} zh_msg_zl_onoff_t;

typedef struct {
	zh_dev_t dev_info;
	uint8_t hue;
	uint8_t sat;
} zh_msg_zl_color_t;

extern void zh_process_init(void);
extern uint16_t http_ssi(int iIndex, char *pcInsert, int iInsertLen);
extern void light_ctrl(int id, int act, int hue, int sat);

#endif
