/**************************************************************************************************
  Filename:       zll_ziglight.h
  Revised:        $Date: 2012-11-29 09:30:23 -0800 (Thu, 29 Nov 2012) $
  Revision:       $Revision: 32352 $

  Description:    This file contains the Zigbee Cluster Library - Light Link
                  (ZLL) Light Sample Application.


  Copyright 2010-2012 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

#ifndef ZLL_ZIGLIGHT_H
#define ZLL_ZIGLIGHT_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ll.h"

#ifndef ZCL_ON_OFF
#error ZCL_ON_OFF should be globally defined to process on/off commands.
#endif
#ifndef ZCL_IDENTIFY
#error ZCL_IDENTIFY should be globally defined to process identify commands.
#endif
#ifndef ZCL_GROUPS
#error ZCL_GROUPS should be globally defined to process group commands.
#endif
#ifndef ZCL_SCENES
#error ZCL_SCENES should be globally defined to process scene commands.
#endif
#ifndef ZCL_LIGHT_LINK_ENHANCE
#error ZCL_LIGHT_LINK_ENHANCE should be globally defined to process light link commands.
#endif

#include "hw_light_ctrl.h"

#ifdef ZCL_LEVEL_CTRL
#include "zcl_level_ctrl.h"
#endif //ZCL_LEVEL_CTRL

#ifdef ZCL_COLOR_CTRL
#include "zcl_color_ctrl.h"
  #ifndef ZCL_LEVEL_CTRL
    #error ZCL_LEVEL_CTRL should be globally defined in color lighting devices.
  #endif
#endif //ZCL_LEVEL_CTRL

/*********************************************************************
 * CONSTANTS
 */
#define ZIGLIGHT_ENDPOINT            11
#define ZIGLIGHT_NUM_GRPS            0

#ifndef ZLL_DEVICEID
  #ifdef ZCL_COLOR_CTRL
    #define ZLL_DEVICEID  ZLL_DEVICEID_COLOR_LIGHT
    #define HA_DEVICEID   ZCL_HA_DEVICEID_COLORED_DIMMABLE_LIGHT
  #else
    #ifdef ZCL_LEVEL_CTRL
      #define ZLL_DEVICEID  ZLL_DEVICEID_DIMMABLE_LIGHT
      #define HA_DEVICEID   ZCL_HA_DEVICEID_DIMMABLE_LIGHT
    #else
      #define ZLL_DEVICEID  ZLL_DEVICEID_ON_OFF_LIGHT
      #define HA_DEVICEID  ZCL_HA_DEVICEID_ON_OFF_LIGHT
    #endif
  #endif
#endif

/*
  Number of attributes:
  10 (Basic) + 1 (Identify) + 5 (Scene) + 1 (Group) + 4 (On/Off) = 21,
  + 2 (Level) = 23   for Dimmable Light
  + 15 (Color) = 38  for Color Light
  + 9 (3 LEDs) = 47  for HW reference
*/
#ifdef ZCL_COLOR_CTRL
  #ifdef ZLL_HW_LED_LAMP
    #define ZIGLIGHT_NUM_ATTRIBUTES      47
  #else
    #define ZIGLIGHT_NUM_ATTRIBUTES      38
  #endif
#else
  #ifdef ZCL_LEVEL_CTRL
    #define ZIGLIGHT_NUM_ATTRIBUTES      23
  #else
    #define ZIGLIGHT_NUM_ATTRIBUTES      21
  #endif
#endif

#define LIGHT_OFF                            0x00
#define LIGHT_ON                             0x01

// Application Events
#define ZIGLIGHT_IDENTIFY_TIMEOUT_EVT     0x0001
#define ZIGLIGHT_EFFECT_PROCESS_EVT       0x0002
#define ZIGLIGHT_ON_TIMED_OFF_TIMER_EVT   0x0004
#define ZIGLIGHT_LEVEL_PROCESS_EVT        0x0008
#define ZIGLIGHT_COLOR_PROCESS_EVT        0x0010
#define ZIGLIGHT_COLOR_LOOP_PROCESS_EVT   0x0020

/*********************************************************************
 * MACROS
 */
#define SCENE_VALID() zllZigLight_SceneValid = 1;
#define SCENE_INVALID() zllZigLight_SceneValid = 0;

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * VARIABLES
 */
extern SimpleDescriptionFormat_t zllZigLight_SimpleDesc;
extern SimpleDescriptionFormat_t zllZigLight_SimpleDesc2;
extern zclLLDeviceInfo_t zllZigLight_DeviceInfo;

extern CONST zclAttrRec_t zllZigLight_Attrs[];

extern uint8  zllZigLight_OnOff;
extern uint8  zllZigLight_GlobalSceneCtrl;
extern uint16 zllZigLight_OnTime;
extern uint16 zllZigLight_OffWaitTime;
extern zclGeneral_Scene_t  zllZigLight_GlobalScene;

extern uint16 zllZigLight_IdentifyTime;

// Scene Cluster (server) -----------------------------------------------------
extern uint8 zllZigLight_CurrentScene;
extern uint16 zllZigLight_CurrentGroup;
extern uint8 zllZigLight_SceneValid;

/*********************************************************************
 * FUNCTIONS
 */

 /*
  * Initialization for the task
  */
extern void zllZigLight_Init( byte task_id );

/*
 *  Event Process for the task
 */
extern UINT16 zllZigLight_event_loop( byte task_id, UINT16 events );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* ZLL_ZIGLIGHT_H */
