/**************************************************************************************************
  Filename:       zll_ziglight.c
  Revised:        $Date: 2013-02-27 13:51:56 -0800 (Wed, 27 Feb 2013) $
  Revision:       $Revision: 33321 $


  Description:    Zigbee Cluster Library - Light Link (ZLL) Light Sample
                  Application.


  Copyright 2010-2013 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS?WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
  This device will be like a Light device.  This application is not
  intended to be a Light device, but will use the device description
  to implement this sample code.
*********************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "onboard.h"

#include "nwk_util.h"


#include "zll_target.h"
#include "zll_ziglight.h"
#include "zll_effects_ctrl.h"

/* HAL */
#include "hal_lcd.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_timer.h"

#include "DebugTrace.h"

#ifdef HAL_BOARD_ZLIGHT
#include "osal_clock.h"
#else
// NWK key printout
#include "osal_nv.h"
#endif

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */
#define FACTORY_RESET_KEY     HAL_KEY_RIGHT
#define CLASSIC_COMMISS_KEY   HAL_KEY_UP
#define PERMIT_JOIN_KEY       HAL_KEY_DOWN
#define DISPLAY_NWK_KEY_KEY   HAL_KEY_LEFT

#define KEY_HOLD_SHORT_INTERVAL    1
#define KEY_HOLD_LONG_INTERVAL     5
#define PERMIT_JOIN_DURATION       60


/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
byte zllZigLight_TaskID;

/*********************************************************************
 * GLOBAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void zllZigLight_HandleKeys( byte shift, byte keys );
static void zllZigLight_BasicResetCB( void );
static void zllZigLight_IdentifyCB( zclIdentify_t *pCmd );
static void zllZigLight_IdentifyQueryRspCB( zclIdentifyQueryRsp_t *pRsp );
static void zllZigLight_OnOffCB( uint8 cmd );
static void zllZigLight_OnOff_OffWithEffectCB( zclOffWithEffect_t *pCmd );
static void zllZigLight_OnOff_OnWithRecallGlobalSceneCB( void );
static void zllZigLight_OnOff_OnWithTimedOffCB( zclOnWithTimedOff_t *pCmd );
static void zllZigLight_ProcessIdentifyTimeChange( void );
static void zllZigLight_ProcessOnWithTimedOffTimer( void );
static void zllZigLight_IdentifyEffectCB( zclIdentifyTriggerEffect_t *pCmd );

// Functions to process ZCL Foundation incoming Command/Response messages
static void zllZigLight_ProcessIncomingMsg( zclIncomingMsg_t *msg );
#ifdef ZCL_READ
static uint8 zllZigLight_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg );
#endif
#ifdef ZCL_WRITE
static uint8 zllZigLight_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg );
#endif
static uint8 zllZigLight_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg );
#ifdef ZCL_DISCOVER
static uint8 zllZigLight_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg );
#endif

// This callback is called to process attribute not handled in ZCL
static ZStatus_t zllZigLight_AttrReadWriteCB( uint16 clusterId, uint16 attrId,
                                       uint8 oper, uint8 *pValue, uint16 *pLen );

static uint8 zllZigLight_SceneStoreCB( zclSceneReq_t *pReq );
static void zllZigLight_SceneRecallCB( zclSceneReq_t *pReq );

#if ( HAL_LCD == TRUE )
static void zllZigLight_PrintNwkKey( uint8 reverse );
#endif

/*********************************************************************
 * ZCL General Profile Callback table
 */
static zclGeneral_AppCallbacks_t zllZigLight_GenCmdCBs =
{
  zllZigLight_BasicResetCB,            // Basic Cluster Reset command
  zllZigLight_IdentifyCB,              // Identify command
  zllZigLight_IdentifyEffectCB,        // Identify Trigger Effect command
  zllZigLight_IdentifyQueryRspCB,      // Identify Query Response command
  zllZigLight_OnOffCB,                 // On/Off cluster commands
  zllZigLight_OnOff_OffWithEffectCB,   // On/Off cluster enhanced command Off with Effect
  zllZigLight_OnOff_OnWithRecallGlobalSceneCB, // On/Off cluster enhanced command On with Recall Global Scene
  zllZigLight_OnOff_OnWithTimedOffCB,  // On/Off cluster enhanced command On with Timed Off
#ifdef ZCL_LEVEL_CTRL
  zclLevel_MoveToLevelCB,                 // Level Control Move to Level command
  zclLevel_MoveCB,                        // Level Control Move command
  zclLevel_StepCB,                        // Level Control Step command
  zclLevel_StopCB,                        // Level Control Stop command
#else
  NULL,                                   // Level Control Move to Level command
  NULL,                                   // Level Control Move command
  NULL,                                   // Level Control Step command
  NULL,                                   // Level Control Stop command
#endif
  NULL,                                   // Group Response commands
  zllZigLight_SceneStoreCB,            // Scene Store Request command
  zllZigLight_SceneRecallCB,           // Scene Recall Request command
  NULL,                                   // Scene Response command
  NULL,                                   // Alarm (Response) commands
  NULL,                                   // RSSI Location command
  NULL                                    // RSSI Location Response command
};

#ifdef ZCL_COLOR_CTRL
static zclLighting_AppCallbacks_t zllZigLight_LightingCmdCBs =
{
  zclColor_MoveToHueCB,   //Move To Hue Command
  zclColor_MoveHueCB,   //Move Hue Command
  zclColor_StepHueCB,   //Step Hue Command
  zclColor_MoveToSaturationCB,   //Move To Saturation Command
  zclColor_MoveSaturationCB,   //Move Saturation Command
  zclColor_StepSaturationCB,   //Step Saturation Command
  zclColor_MoveToHueAndSaturationCB,   //Move To Hue And Saturation  Command
  zclColor_MoveToColorCB, // Move To Color Command
  zclColor_MoveColorCB,   // Move Color Command
  zclColor_StepColorCB,   // STEP To Color Command
  NULL,                                     // Move To Color Temperature Command
  zclColor_EnhMoveToHueCB,// Enhanced Move To Hue
  zclColor_MoveEnhHueCB,  // Enhanced Move Hue;
  zclColor_StepEnhHueCB,  // Enhanced Step Hue;
  zclColor_MoveToEnhHueAndSaturationCB, // Enhanced Move To Hue And Saturation;
  zclColor_SetColorLoopCB, // Color Loop Set Command
  zclColor_StopCB,        // Stop Move Step;
};
#endif //ZCL_COLOR_CTRL

/*********************************************************************
 * @fn          zllZigLight_Init
 *
 * @brief       Initialization function for the Sample Light App Task.
 *
 * @param       task_id
 *
 * @return      none
 */
void zllZigLight_Init( byte task_id )
{
  zllZigLight_TaskID = task_id;

  // Register the ZCL General Cluster Library callback functions
  zclGeneral_RegisterCmdCallbacks( ZIGLIGHT_ENDPOINT, &zllZigLight_GenCmdCBs );

#ifdef ZLL_HW_LED_LAMP
  HalTimer1Init(0);
#endif //ZLL_HW_LED_LAMP

  zllEffects_init(zllZigLight_TaskID, zllZigLight_OnOffCB);

#ifdef ZCL_LEVEL_CTRL
  zclLevel_init(zllZigLight_TaskID, zllZigLight_OnOffCB);
#else
  #ifdef ZLL_HW_LED_LAMP
    halTimer1SetChannelDuty (WHITE_LED, PWM_FULL_DUTY_CYCLE); //initialize on/off LED to full power
  #endif
#endif //ZCL_LEVEL_CTRL

#ifdef ZCL_COLOR_CTRL
  // Register the ZCL Lighting Cluster Library callback functions
  zclLighting_RegisterCmdCallbacks( ZIGLIGHT_ENDPOINT, &zllZigLight_LightingCmdCBs );
  zclColor_init(zllZigLight_TaskID);
#endif //#ifdef ZCL_COLOR_CTRL

  // Register the application's attribute list
  zcl_registerAttrList( ZIGLIGHT_ENDPOINT, ZIGLIGHT_NUM_ATTRIBUTES, zllZigLight_Attrs );

  // Register the application's callback function to read the Scene Count attribute.
  zcl_registerReadWriteCB( ZIGLIGHT_ENDPOINT, zllZigLight_AttrReadWriteCB, NULL );

  // Register for all key events - This app will handle all key events
  RegisterForKeys( zllZigLight_TaskID );

  zllTarget_RegisterApp( &zllZigLight_SimpleDesc, &zllZigLight_DeviceInfo );

  zllTarget_RegisterIdentifyCB( zllZigLight_IdentifyCB );

  zllTarget_InitDevice();

  zllZigLight_OnOffCB( zllZigLight_OnOff );
}

/*********************************************************************
 * @fn          zclSample_event_loop
 *
 * @brief       Event Loop Processor for the Sample Light App Task.
 *
 * @param       task_id
 * @param       events - events bitmap
 *
 * @return      unprocessed events bitmap
 */
uint16 zllZigLight_event_loop( uint8 task_id, uint16 events )
{
  (void)task_id;  // Intentionally unreferenced parameter

  if ( events & SYS_EVENT_MSG )
  {
    afIncomingMSGPacket_t *pMsg;

    if ( (pMsg = (afIncomingMSGPacket_t *)osal_msg_receive( zllZigLight_TaskID )) )
    {
      switch ( pMsg->hdr.event )
      {
        case ZCL_INCOMING_MSG:
          // Incoming ZCL Foundation command/response messages
          zllZigLight_ProcessIncomingMsg( (zclIncomingMsg_t *)pMsg );
          break;

        case KEY_CHANGE:
          zllZigLight_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
          break;

        default:
          break;
      }

      // Release the memory
      osal_msg_deallocate( (uint8 *)pMsg );
    }

    // return unprocessed events
    return (events ^ SYS_EVENT_MSG);
  }

  if ( events & ZIGLIGHT_IDENTIFY_TIMEOUT_EVT )
  {
    if ( zllZigLight_IdentifyTime > 0 )
      zllZigLight_IdentifyTime--;
    zllZigLight_ProcessIdentifyTimeChange();

    return ( events ^ ZIGLIGHT_IDENTIFY_TIMEOUT_EVT );
  }

  if ( events & ZIGLIGHT_EFFECT_PROCESS_EVT )
  {
    zllEffects_ProcessEffect();
    return ( events ^ ZIGLIGHT_EFFECT_PROCESS_EVT );
  }

  if ( events & ZIGLIGHT_ON_TIMED_OFF_TIMER_EVT )
  {
    zllZigLight_ProcessOnWithTimedOffTimer();
    return ( events ^ ZIGLIGHT_ON_TIMED_OFF_TIMER_EVT );
  }

#ifdef ZCL_LEVEL_CTRL
    //update the level
    zclLevel_process(&events);
#endif //ZCL_COLOR_CTRL

#ifdef ZCL_COLOR_CTRL
    //update the color
    zclColor_process(&events);
    zclColor_processColorLoop(&events);
#endif //ZCL_COLOR_CTRL


  // Discard unknown events
  return 0;
}

/*********************************************************************
 * @fn      zllZigLight_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events.
 *
 * @return  none
 */
static void zllZigLight_HandleKeys( byte shift, byte keys )
{
  (void)shift;  // Intentionally unreferenced parameter

  if ( keys & HAL_KEY_SW_1 )
  {
    debug_str("Reset Key Pushed!");
    
    NLME_InitNV();
    NLME_SetDefaultNV();
    
    SystemResetSoft();
  }
}

#if (HAL_LCD == TRUE)
/*********************************************************************
 * @fn      zllZigLight_PrintNwkKey
 *
 * @brief   Print current NWK key on the LCD screen
 *
 * @param   reverse - TRUE to print the bytes in reverse order
 *
 * @return  none
 */
static void zllZigLight_PrintNwkKey( uint8 reverse )
{
  nwkKeyDesc KeyInfo;
  uint8 lcd_buf[SEC_KEY_LEN+1];
  uint8 lcd_buf1[SEC_KEY_LEN+1];
  uint8 lcd_buf2[SEC_KEY_LEN+1];
  uint8 i;

  // Swap active and alternate keys writing them directly to NV
  osal_nv_read(ZCD_NV_NWK_ACTIVE_KEY_INFO, 0,
               sizeof(nwkKeyDesc), &KeyInfo);

  for (i = 0; i < SEC_KEY_LEN/2; i++)
  {
    uint8 key = KeyInfo.key[i];
    _itoa(KeyInfo.key[i], &lcd_buf[i*2], 16);
    if( (key & 0xF0) == 0)
    {
      //_itoa shift to significant figures, so swaps bytes if 0x0?
      lcd_buf[(i*2) + 1] = lcd_buf[i*2];
      lcd_buf[i*2] = '0';
    }
    if( (key & 0xF) == 0)
    {
      lcd_buf[(i*2) + 1] = '0';
    }
  }

  if ( reverse )
  {
    //print in daintree format. Reverse bytes.
    for (i = 0; i < (SEC_KEY_LEN); i+=2)
    {
      lcd_buf2[SEC_KEY_LEN - (i+2)] = lcd_buf[i];
      lcd_buf2[SEC_KEY_LEN - (i+1)] = lcd_buf[i+1];
    }
  }
  else
  {
    osal_memcpy(lcd_buf1, lcd_buf, SEC_KEY_LEN);
  }

  for (i = 0; i < SEC_KEY_LEN/2; i++)
  {
    uint8 key = KeyInfo.key[i + SEC_KEY_LEN/2];
    _itoa(KeyInfo.key[i + SEC_KEY_LEN/2], &lcd_buf[i*2], 16);
    if( (key & 0xF0) == 0)
    {
      //_itoa shift to significant figures, so swaps bytes if 0x0?
      lcd_buf[(i*2) + 1] = lcd_buf[i*2];
      lcd_buf[i*2] = '0';
    }
    if( (key & 0xF) == 0)
    {
      lcd_buf[(i*2) + 1] = '0';
    }
  }

  if ( reverse )
  {
    //print in daintree format. Reverse bytes.
    for (i = 0; i < (SEC_KEY_LEN); i+=2)
    {
      lcd_buf1[SEC_KEY_LEN - (i+2)] = lcd_buf[i];
      lcd_buf1[SEC_KEY_LEN - (i+1)] = lcd_buf[i+1];
    }
  }
  else
  {
    osal_memcpy(lcd_buf2, lcd_buf, SEC_KEY_LEN);
  }

  lcd_buf1[SEC_KEY_LEN] = '\0';
  lcd_buf2[SEC_KEY_LEN] = '\0';
  if ( reverse )
  {
    HalLcdWriteString( "NWK KEY (Rev.):", HAL_LCD_LINE_1 );
  }
  else
  {
    HalLcdWriteString( "NWK KEY: ", HAL_LCD_LINE_1 );
  }
  HalLcdWriteString( (char*)lcd_buf1, HAL_LCD_LINE_2 );
  HalLcdWriteString( (char*)lcd_buf2, HAL_LCD_LINE_3 );
}
#endif //(HAL_LCD == TRUE)

/*********************************************************************
 * @fn      zllZigLight_ProcessIdentifyTimeChange
 *
 * @brief   Called to process any change to the IdentifyTime attribute.
 *
 * @param   none
 *
 * @return  none
 */
static void zllZigLight_ProcessIdentifyTimeChange( void )
{
  if ( zllZigLight_IdentifyTime > 0 )
  {
    osal_start_timerEx( zllZigLight_TaskID, ZIGLIGHT_IDENTIFY_TIMEOUT_EVT, 1000 );
    zllEffects_Blink(TRUE);
  }
  else
  {
    zllEffects_Blink(FALSE);
  }
}

/*********************************************************************
 * @fn      zllZigLight_ProcessOnWithTimedOffTimer
 *
 * @brief   Called to process On with Timed Off attributes changes over time.
 *
 * @param   none
 *
 * @return  none
 */
static void zllZigLight_ProcessOnWithTimedOffTimer( void )
{
  if ( ( zllZigLight_OnOff == LIGHT_ON ) && ( zllZigLight_OnTime > 0 ) )
  {
    zllZigLight_OnTime--;
    if ( zllZigLight_OnTime <= 0 )
    {
      zllZigLight_OffWaitTime = 0x00;
      zllZigLight_OnOffCB( COMMAND_OFF );
    }
  }
  if ( ( zllZigLight_OnOff == LIGHT_OFF ) && ( zllZigLight_OffWaitTime > 0 ) )
  {
    zllZigLight_OffWaitTime--;
    if ( zllZigLight_OffWaitTime <= 0 )
    {
      osal_stop_timerEx( zllZigLight_TaskID, ZIGLIGHT_ON_TIMED_OFF_TIMER_EVT);
      return;
    }
  }

  if ( ( zllZigLight_OnTime > 0 ) || ( zllZigLight_OffWaitTime > 0 ) )
  {
    osal_start_timerEx( zllZigLight_TaskID, ZIGLIGHT_ON_TIMED_OFF_TIMER_EVT, 100 );
  }
}

/*********************************************************************
 * @fn      zllZigLight_AttrReadWriteCB
 *
 * @brief   Read/write callbackfor read/wtire attrs tha have NULL dataPtr
 *
 *          Note: This function gets called only when the pointer
 *                'dataPtr' to the Scene Count attribute value is
 *                NULL in the attribute database registered with
 *                the ZCL.
 *
 * @param   clusterId - cluster that attribute belongs to
 * @param   attrId - attribute to be read or written
 * @param   oper - ZCL_OPER_LEN, ZCL_OPER_READ, or ZCL_OPER_WRITE
 * @param   pValue - pointer to attribute value
 * @param   pLen - length of attribute value read
 *
 * @return  status
 */
ZStatus_t zllZigLight_AttrReadWriteCB( uint16 clusterId, uint16 attrId,
                                       uint8 oper, uint8 *pValue, uint16 *pLen )
{
  ZStatus_t status = ZCL_STATUS_SUCCESS;

#if defined ZCL_SCENES
  //SceneCount Attr
  if( (clusterId == ZCL_CLUSTER_ID_GEN_SCENES) &&
     (attrId == ATTRID_SCENES_COUNT) )
  {
    status = zclGeneral_ReadSceneCountCB(clusterId, attrId, oper, pValue, pLen);
  } else
#endif //ZCL_SCENES
  //IdentifyTime Attr
  if( (clusterId == ZCL_CLUSTER_ID_GEN_IDENTIFY) &&
     (attrId == ATTRID_IDENTIFY_TIME) )
  {
    switch ( oper )
    {
      case ZCL_OPER_LEN:
        *pLen = 2; // uint16
        break;

      case ZCL_OPER_READ:
        pValue[0] = LO_UINT16( zllZigLight_IdentifyTime );
        pValue[1] = HI_UINT16( zllZigLight_IdentifyTime );

        if ( pLen != NULL )
        {
          *pLen = 2;
        }
        break;

      case ZCL_OPER_WRITE:
      {
        zclIdentify_t cmd;
        cmd.identifyTime = BUILD_UINT16( pValue[0], pValue[1] );

        zllZigLight_IdentifyCB( &cmd );

        break;
      }

      default:
        status = ZCL_STATUS_SOFTWARE_FAILURE; // should never get here!
        break;
    }
  }
  else
  {
    status = ZCL_STATUS_SOFTWARE_FAILURE; // should never get here!
  }

  return ( status );
}
/*********************************************************************
 * @fn      zllZigLight_BasicResetCB
 *
 * @brief   Callback from the ZCL General Cluster Library
 *          to set all the Basic Cluster attributes to default values.
 *
 * @param   none
 *
 * @return  none
 */
static void zllZigLight_BasicResetCB( void )
{
  // Reset all attributes to default values
}

/*********************************************************************
 * @fn      zllZigLight_IdentifyCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received an Identity Command for this application.
 *
 * @param   srcAddr - source address and endpoint of the response message
 * @param   identifyTime - the number of seconds to identify yourself
 *
 * @return  none
 */
static void zllZigLight_IdentifyCB( zclIdentify_t *pCmd )
{
  zllZigLight_IdentifyTime = pCmd->identifyTime;
  zllZigLight_ProcessIdentifyTimeChange();
}


/*********************************************************************
 * @fn      zllZigLight_IdentifyCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received an Identity Command for this application.
 *
 * @param   srcAddr - source address and endpoint of the response message
 * @param   identifyTime - the number of seconds to identify yourself
 *
 * @return  none
 */
static void zllZigLight_IdentifyEffectCB( zclIdentifyTriggerEffect_t *pCmd )
{
  HalLcdWriteStringValue( "IdentifyEffId", pCmd->effectId, 16, HAL_LCD_LINE_1 );
  zllEffects_initiateEffect( ZCL_CLUSTER_ID_GEN_IDENTIFY, COMMAND_IDENTIFY_TRIGGER_EFFECT,
                                 pCmd->effectId, pCmd->effectVariant );
}

/*********************************************************************
 * @fn      zllZigLight_IdentifyQueryRspCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received an Identity Query Response Command for this application.
 *
 * @param   srcAddr - requestor's address
 * @param   timeout - number of seconds to identify yourself (valid for query response)
 *
 * @return  none
 */
static void zllZigLight_IdentifyQueryRspCB(  zclIdentifyQueryRsp_t *pRsp )
{
  // Query Response (with timeout value)
  (void)pRsp;
}

/*********************************************************************
 * @fn      zllZigLight_OnOffCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received an On/Off Command for this application.
 *
 * @param   cmd - COMMAND_ON, COMMAND_OFF, or COMMAND_TOGGLE
 *
 * @return  none
 */
static void zllZigLight_OnOffCB( uint8 cmd )
{
  // Turn on the light
  if ( cmd == COMMAND_ON )
  {
    zllZigLight_OnOff = LIGHT_ON;
    zllZigLight_GlobalSceneCtrl = TRUE;
    if ( zllZigLight_OnTime == 0 )
    {
      zllZigLight_OffWaitTime = 0;
    }
  }

  // Turn off the light
  else if ( cmd == COMMAND_OFF )
  {
    zllZigLight_OnOff = LIGHT_OFF;
    //zllZigLight_GlobalSceneCtrl = FALSE; //see ZLL spec 11-0037-03 6.6.1.2.1
    zllZigLight_OnTime = 0;
  }

  // Toggle the light
  else
  {
    if ( zllZigLight_OnOff == LIGHT_OFF )
    {
      zllZigLight_OnOff = LIGHT_ON;
      zllZigLight_GlobalSceneCtrl = TRUE;
      if ( zllZigLight_OnTime == 0 )
      {
        zllZigLight_OffWaitTime = 0;
      }
    }
    else
    {
      zllZigLight_OnOff = LIGHT_OFF;
      zllZigLight_OnTime = 0;
    }
  }

  // In this sample app, we use LED4 to simulate the Light
  if ( zllZigLight_OnOff == LIGHT_ON )
  {
#ifdef ZLL_HW_LED_LAMP
    ENABLE_LAMP;
#else
    HalLedSet( HAL_LED_1, HAL_LED_MODE_ON );
#endif
  }
  else
  {
#ifdef ZLL_HW_LED_LAMP
    DISABLE_LAMP;
#else
    HalLedSet( HAL_LED_1, HAL_LED_MODE_OFF );
#endif
  }
  zllZigLight_SceneValid = 0;

}


/*********************************************************************
 * @fn      zllZigLight_OffWithEffect
 *
 * @brief   Callback from the ZCL General Cluster Library when it
 *          received an Off with Effect Command for this application.
 *
 * @param   pCmd - Off with Effect parameters
 *
 * @return  none
 */
static void zllZigLight_OnOff_OffWithEffectCB( zclOffWithEffect_t *pCmd )
{
  HalLcdWriteStringValueValue( "OffWithEff", pCmd->effectId, 16,
                               pCmd->effectVariant, 16, HAL_LCD_LINE_1 );
  if( zllZigLight_GlobalSceneCtrl )
  {
    zclSceneReq_t req;
    req.scene = &zllZigLight_GlobalScene;

    zllZigLight_SceneStoreCB( &req );
    zllZigLight_GlobalSceneCtrl = FALSE;
  }
  zllEffects_initiateEffect( ZCL_CLUSTER_ID_GEN_ON_OFF, COMMAND_OFF_WITH_EFFECT,
                                 pCmd->effectId, pCmd->effectVariant );
}

/*********************************************************************
 * @fn      zllZigLight_OnOff_OnWithRecallGlobalSceneCB
 *
 * @brief   Callback from the ZCL General Cluster Library when it
 *          received an On with Recall Global Scene Command for this application.
 *
 * @param   none
 *
 * @return  none
 */
static void zllZigLight_OnOff_OnWithRecallGlobalSceneCB( void )
{
  if( !zllZigLight_GlobalSceneCtrl )
  {
    zclSceneReq_t req;
    req.scene = &zllZigLight_GlobalScene;

    zllZigLight_SceneRecallCB( &req );

    zllZigLight_GlobalSceneCtrl = TRUE;
  }
  else  // Turn on the light
  {
    zllZigLight_OnOffCB( COMMAND_ON );
  }
}

/*********************************************************************
 * @fn      zllZigLight_OnOff_OnWithTimedOffCB
 *
 * @brief   Callback from the ZCL General Cluster Library when it
 *          received an On with Timed Off Command for this application.
 *
 * @param   pCmd - On with Timed Off parameters
 *
 * @return  none
 */
static void zllZigLight_OnOff_OnWithTimedOffCB( zclOnWithTimedOff_t *pCmd )
{
  if ( ( pCmd->onOffCtrl.bits.acceptOnlyWhenOn == TRUE )
      && ( zllZigLight_OnOff == LIGHT_OFF ) )
  {
    return;
  }

  if ( ( zllZigLight_OffWaitTime > 0 ) && ( zllZigLight_OnOff == LIGHT_OFF ) )
  {
    zllZigLight_OffWaitTime = MIN( zllZigLight_OffWaitTime, pCmd->offWaitTime );
  }
  else
  {
    uint16 maxOnTime = MAX( zllZigLight_OnTime, pCmd->onTime );
    zllZigLight_OnOffCB( COMMAND_ON );
    zllZigLight_OnTime = maxOnTime;
    zllZigLight_OffWaitTime = pCmd->offWaitTime;
  }

  if ( ( zllZigLight_OnTime < 0xFFFF ) && ( zllZigLight_OffWaitTime < 0xFFFF ) )
  {
    osal_start_timerEx( zllZigLight_TaskID, ZIGLIGHT_ON_TIMED_OFF_TIMER_EVT, 100 );
  }
}

/******************************************************************************
 *
 *  Functions for processing ZCL Foundation incoming Command/Response messages
 *
 *****************************************************************************/

/*********************************************************************
 * @fn      zllZigLight_ProcessIncomingMsg
 *
 * @brief   Process ZCL Foundation incoming message
 *
 * @param   pInMsg - pointer to the received message
 *
 * @return  none
 */
static void zllZigLight_ProcessIncomingMsg( zclIncomingMsg_t *pInMsg)
{
  switch ( pInMsg->zclHdr.commandID )
  {
#ifdef ZCL_READ
    case ZCL_CMD_READ_RSP:
      zllZigLight_ProcessInReadRspCmd( pInMsg );
      break;
#endif
#ifdef ZCL_WRITE
    case ZCL_CMD_WRITE_RSP:
      zllZigLight_ProcessInWriteRspCmd( pInMsg );
      break;
#endif
#ifdef ZCL_REPORT
    // See ZCL Test Applicaiton (zcl_testapp.c) for sample code on Attribute Reporting
    case ZCL_CMD_CONFIG_REPORT:
      //zllZigLight_ProcessInConfigReportCmd( pInMsg );
      break;

    case ZCL_CMD_CONFIG_REPORT_RSP:
      //zllZigLight_ProcessInConfigReportRspCmd( pInMsg );
      break;

    case ZCL_CMD_READ_REPORT_CFG:
      //zllZigLight_ProcessInReadReportCfgCmd( pInMsg );
      break;

    case ZCL_CMD_READ_REPORT_CFG_RSP:
      //zllZigLight_ProcessInReadReportCfgRspCmd( pInMsg );
      break;

    case ZCL_CMD_REPORT:
      //zllZigLight_ProcessInReportCmd( pInMsg );
      break;
#endif
    case ZCL_CMD_DEFAULT_RSP:
      zllZigLight_ProcessInDefaultRspCmd( pInMsg );
      break;
#ifdef ZCL_DISCOVER
    case ZCL_CMD_DISCOVER_RSP:
      zllZigLight_ProcessInDiscRspCmd( pInMsg );
      break;
#endif
    default:
      break;
  }

  if ( pInMsg->attrCmd )
    osal_mem_free( pInMsg->attrCmd );
}

#ifdef ZCL_READ
/*********************************************************************
 * @fn      zllZigLight_ProcessInReadRspCmd
 *
 * @brief   Process the "Profile" Read Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zllZigLight_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg )
{
  zclReadRspCmd_t *readRspCmd;
  uint8 i;

  readRspCmd = (zclReadRspCmd_t *)pInMsg->attrCmd;
  for (i = 0; i < readRspCmd->numAttr; i++)
  {
    // Notify the originator of the results of the original read attributes
    // attempt and, for each successfull request, the value of the requested
    // attribute
  }

  return TRUE;
}
#endif // ZCL_READ

#ifdef ZCL_WRITE
/*********************************************************************
 * @fn      zllZigLight_ProcessInWriteRspCmd
 *
 * @brief   Process the "Profile" Write Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zllZigLight_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg )
{
  zclWriteRspCmd_t *writeRspCmd;
  uint8 i;

  writeRspCmd = (zclWriteRspCmd_t *)pInMsg->attrCmd;
  for (i = 0; i < writeRspCmd->numAttr; i++)
  {
    // Notify the device of the results of the its original write attributes
    // command.
  }

  return TRUE;
}
#endif // ZCL_WRITE

/*********************************************************************
 * @fn      zllZigLight_ProcessInDefaultRspCmd
 *
 * @brief   Process the "Profile" Default Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zllZigLight_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg )
{
  // zclDefaultRspCmd_t *defaultRspCmd = (zclDefaultRspCmd_t *)pInMsg->attrCmd;

  // Device is notified of the Default Response command.
  (void)pInMsg;

  return TRUE;
}

#ifdef ZCL_DISCOVER
/*********************************************************************
 * @fn      zllZigLight_ProcessInDiscRspCmd
 *
 * @brief   Process the "Profile" Discover Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zllZigLight_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg )
{
  zclDiscoverRspCmd_t *discoverRspCmd;
  uint8 i;

  discoverRspCmd = (zclDiscoverRspCmd_t *)pInMsg->attrCmd;
  for ( i = 0; i < discoverRspCmd->numAttr; i++ )
  {
    // Device is notified of the result of its attribute discovery command.
  }

  return TRUE;
}
#endif // ZCL_DISCOVER

/*********************************************************************
 * @fn      zllZigLight_SceneStoreCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received a Scene Store Request Command for
 *          this application.
 *          Extension field sets =
 *          {{Cluster ID 1, length 1, {extension field set 1}}, {{Cluster ID 2,
 *            length 2, {extension field set 2}}, ...}
 *
 * @param   srcAddr -
 * @param   scene -
 *
 * @return  TRUE if extField is filled out, FALSE if not filled
 *          and there is no need to save the scene
 */
static uint8 zllZigLight_SceneStoreCB( zclSceneReq_t *pReq )
{
  uint8 *pExt;

  // The length of the extension field(s) is 23:
  //   2 + 1 + 1 for On/Off cluster (onOff attibute)
  //   2 + 1 + 1 for Level Control cluster (currentLevel attribute)
  //   2 + 1 + 12 for Color Control cluster (CurrentHue/CurrentSaturation/currentX/currentY/EnhancedCurrentHue/
  //                                         colorLoopActive/colorLoopDirection/colorLoopTime attributes)

  if ( pReq->scene->extLen != ZCL_GEN_SCENE_EXT_LEN )
  {
    pReq->scene->extLen = ZCL_GEN_SCENE_EXT_LEN;
  }

  pExt = pReq->scene->extField;

  // Build an extension field for On/Off cluster
  *pExt++ = LO_UINT16( ZCL_CLUSTER_ID_GEN_ON_OFF );
  *pExt++ = HI_UINT16( ZCL_CLUSTER_ID_GEN_ON_OFF );
  *pExt++ = 1; // length

  // Store the value of onOff attribute
  *pExt++ = zllZigLight_OnOff;

#ifdef ZCL_LEVEL_CTRL
  // Build an extension field for Level Control cluster
  *pExt++ = LO_UINT16( ZCL_CLUSTER_ID_GEN_LEVEL_CONTROL );
  *pExt++ = HI_UINT16( ZCL_CLUSTER_ID_GEN_LEVEL_CONTROL );
  *pExt++ = 1; // length

  // Store the value of currentLevel attribute
  *pExt++ = zclLevel_CurrentLevel;
#endif //ZCL_LEVEL_CTRL

#ifdef ZCL_COLOR_CTRL
  // Build an extension field for Color Control cluster
  *pExt++ = LO_UINT16( ZCL_CLUSTER_ID_LIGHTING_COLOR_CONTROL );
  *pExt++ = HI_UINT16( ZCL_CLUSTER_ID_LIGHTING_COLOR_CONTROL );
  *pExt++ = 12; // length

  *pExt++ = zclColor_CurrentHue;
  *pExt++ = zclColor_CurrentSaturation;
  *pExt++ = LO_UINT16( zclColor_CurrentX );
  *pExt++ = HI_UINT16( zclColor_CurrentX );
  *pExt++ = LO_UINT16( zclColor_CurrentY );
  *pExt++ = HI_UINT16( zclColor_CurrentY );
  *pExt++ = LO_UINT16( zclColor_EnhancedCurrentHue );
  *pExt++ = HI_UINT16( zclColor_EnhancedCurrentHue );
  *pExt++ = zclColor_ColorLoopActive;
  *pExt++ = zclColor_ColorLoopDirection;
  *pExt++ = LO_UINT16( zclColor_ColorLoopTime );
  *pExt++ = HI_UINT16( zclColor_ColorLoopTime );
#endif //ZCL_COLOR_CTRL

  // Add more cluster

  return ( TRUE );
}

/*********************************************************************
 * @fn      zllZigLight_SceneRecallCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received a Scene Recall Request Command for
 *          this application.
 *          Extension field sets =
 *          {{Cluster ID 1, length 1, {extension field set 1}}, {{Cluster ID 2,
 *            length 2, {extension field set 2}}, ...}
 *
 * @param   srcAddr -
 * @param   scene -
 *
 * @return  none
 */
static void zllZigLight_SceneRecallCB( zclSceneReq_t *pReq )
{
  uint8 len;
  uint16 clusterID;
  uint8 *pExt = pReq->scene->extField;

  while ( pExt < pReq->scene->extField + pReq->scene->extLen )
  {
    clusterID =  BUILD_UINT16( pExt[0], pExt[1] );
    pExt += 2; // cluster ID
    len = *pExt++;

    if ( clusterID == ZCL_CLUSTER_ID_GEN_ON_OFF )
    {
      // Update onOff attibute with the recalled value
      if ( len == 1 )
        zllZigLight_OnOffCB( *pExt++ ); // Apply the new value
    }
    else if ( clusterID == ZCL_CLUSTER_ID_GEN_LEVEL_CONTROL )
    {
      // Update currentLevel attribute with the recalled value
      if ( len == 1 )
      {
#ifdef ZCL_LEVEL_CTRL
        uint8 newCurrentLevel;
        zclLCMoveToLevel_t levelCmd;

        newCurrentLevel = *pExt++;

        levelCmd.level = newCurrentLevel;
        levelCmd.transitionTime = 0; //0 for now, need to use transition time
        levelCmd.withOnOff = 0;
        zclLevel_MoveToLevelCB( &levelCmd );
#else  //ZCL_LEVEL_CTRL
        *pExt++;
#endif //ZCL_LEVEL_CTRL
      }
    }
    else if ( clusterID == ZCL_CLUSTER_ID_LIGHTING_COLOR_CONTROL )
    {
      // Update currentX and currentY attributes with the recalled values
      if ( len == 12 )
      {
#ifdef ZCL_COLOR_CTRL
        uint8  newCurrentHue, newCurrentSaturation;
        uint16 newCurrentX, newCurrentY, newEnhancedCurrentHue;
        zclCCColorLoopSet_t newColorLoopSetCmd = {0};

        newCurrentHue = *pExt++;
        newCurrentSaturation = *pExt++;
        newCurrentX = BUILD_UINT16( pExt[0], pExt[1] );
        pExt += 2;
        newCurrentY = BUILD_UINT16( pExt[0], pExt[1] );
        pExt += 2;
        newEnhancedCurrentHue = BUILD_UINT16( pExt[0], pExt[1] );
        pExt += 2;

        newColorLoopSetCmd.updateFlags.bits.action = TRUE;
        newColorLoopSetCmd.action = ( (*pExt++) ? LIGHTING_COLOR_LOOP_ACTION_ACTIVATE_FROM_ENH_CURR_HUE : LIGHTING_COLOR_LOOP_ACTION_DEACTIVATE );
        newColorLoopSetCmd.updateFlags.bits.direction = TRUE;
        newColorLoopSetCmd.direction = *pExt++;
        newColorLoopSetCmd.updateFlags.bits.time = TRUE;
        newColorLoopSetCmd.time = BUILD_UINT16( pExt[0], pExt[1] );
        pExt += 2;

        if ( zclColor_ColorMode == COLOR_MODE_CURRENT_HUE_SATURATION )
        {
          if ( zclColor_EnhancedColorMode == ENHANCED_COLOR_MODE_ENHANCED_CURRENT_HUE_SATURATION )
          {
            zclCCEnhancedMoveToHueAndSaturation_t cmd;
            cmd.enhancedHue = newEnhancedCurrentHue;
            cmd.saturation = newCurrentSaturation;
            cmd.transitionTime = 0; //0 for now, need to use transition time
            zclColor_MoveToEnhHueAndSaturationCB( &cmd );
            zclColor_CurrentHue = newCurrentHue;
          }
          else //ENHANCED_COLOR_MODE_CURRENT_HUE_SATURATION
          {
            zclCCMoveToHueAndSaturation_t cmd;
            cmd.hue = newCurrentHue;
            cmd.saturation = newCurrentSaturation;
            cmd.transitionTime = 0; //0 for now, need to use transition time
            zclColor_MoveToHueAndSaturationCB( &cmd );
            zclColor_EnhancedCurrentHue = newEnhancedCurrentHue;
          }
          //restore x,y attributes values
          zclColor_CurrentX = newCurrentX;
          zclColor_CurrentY = newCurrentY;
        }
        else
        {
          zclCCMoveToColor_t colorCmd;

          colorCmd.colorX = newCurrentX;
          colorCmd.colorY = newCurrentY;
          colorCmd.transitionTime = 0; //0 for now, need to use transition time

          zclColor_MoveToColorCB( &colorCmd );
          //restore hue,sat attributes values
          zclColor_CurrentHue = newCurrentHue;
          zclColor_EnhancedCurrentHue = newEnhancedCurrentHue;
          zclColor_CurrentSaturation = newCurrentSaturation;
        }
        zclColor_SetColorLoopCB( &newColorLoopSetCmd );

#else  //ZCL_COLOR_CTRL
        pExt += 12;
#endif //ZCL_COLOR_CTRL
      }
    }
    else
    {
      // Add more cluster
    }
  }

  zllZigLight_CurrentScene = pReq->scene->ID;
  zllZigLight_CurrentGroup = pReq->scene->groupID;
  zllZigLight_GlobalSceneCtrl = TRUE;
  SCENE_VALID();
}


/****************************************************************************
****************************************************************************/


