/*******************************************************************************

Filename:     hal_2530.c
Target:       cc2530
Author:       jejer
Revised:      2013-7
Revision:     1.0
Email:        zjejer@gmail.com

******************************************************************************/

#include "hal_2530.h"

WORD halSetTimer1Period(DWORD period){

   BYTE div = 0;

   // Checking that the period is not too short for the timer tick interval.
   if(HAL_TICKSPD > 5) {
      if( (period < 2*(HAL_TICKSPD-5)) && (period != 0) ){
         return 0;
      }
   }

   if(period == 0){  // Max timer period is used.
      div = 3;
      period = 0x0000FFFF;
   }
   else{
      period = ((period*32) >> HAL_TICKSPD); // Calculating number of timer ticks the period
      if(period&0xFFFF0000){             // Using prescaler to fit period
         period = (period >> 3);
         div = 1;
         while (period&0xFFFF0000){
            period = (period >> 2);
            div++;
            if(div > 3){                 // If the period is too long, 0 is returned.
               return 0;
            }
         }
      }
   }

   T1CTL = ((T1CTL&~0x0c) | (div << 2)); // Setting prescaler division value

   T1CC0L = (BYTE)(period);              // Setting counter value
   T1CC0H = (BYTE)(period >> 8);

   return (WORD)period;
}

#if 0
BOOL halSetTimer2Period(BYTE mode, DWORD period){

   if(mode&TIMER2_MAC_TIMER){
      T2CAPHPH = 0x28;  // setting for 320 u-second periods as specified by 802.15.4
      T2CAPLPL = 0x00;  // (0x2800) / 32 = 320 u-seconds
   }
   else {
       T2CAPHPH = 0x7D; // Setting timer to have 1 m-second period
       T2CAPLPL = 0x00; // (0x7D00) / 32 = 1000 u-seconds
   }

   if(period){
       if(period&0xFFF00000) {return 0;}// Setting the number of periods (timer overflows) to generate
       T2PEROF0 = (BYTE) period;        // an interrupt.
       period = (period >> 8);
       T2PEROF1 = (BYTE) period;
       period = ((period >> 8)&0x0F);
       T2PEROF2 = ( T2PEROF2&~0x0F | (BYTE)period );
   }
   return 1;
}
#endif

BYTE halSetTimer34Period(BYTE timer, DWORD period){

        BYTE div = 0;

        if(HAL_TICKSPD > 5) { // Checking that the period is not too short.
        if( (period < 2*(HAL_TICKSPD-5)) && (period != 0) ){
               return 0;
           }
    }

        if(period == 0){  // If period is 0, max period length and max prescaler
                div = 7;  // division is used.
                period = 255;
        } else {
                period = ((period*32) >> HAL_TICKSPD);// Determining how many timer ticks the period consist of
                while(period > 255){              // If the period is too long, the prescaler division is
                        period = (period >> 1);   // increased.
                        div++;
                        if(div > 7){              // If the period is too long when using max prescaler division,
                                return 0;         // 0 is returned.
                        }
                }
        }

        if(timer == 4){
                // Timer 4 selected
                T4CTL |= (div << 5);              // Setting prescaler value
                T4CC0 = (BYTE) period;            // Setting timer value.
        } else if(timer == 3){
                // Timer 3 selected
                T3CTL |= (div << 5);              // Setting prescaler value
                T3CC0 = (BYTE) period;            // Setting timer value.
        } else {return 0;}

        return period;

}

INT16 halAdcSampleSingle(BYTE reference, BYTE resolution, UINT8 input) {
    BYTE volatile temp;
    INT16 value;

    //reading out any old conversion value
    temp = ADCH;
    temp = ADCL;


    ADC_ENABLE_CHANNEL(input);
    ADC_STOP();

    ADC_SINGLE_CONVERSION(reference | resolution | input);

    while (!ADC_SAMPLE_READY());

    ADC_DISABLE_CHANNEL(input);

    value = (((INT16)ADCH) << 8);
    value |= ADCL;
	
    //  The variable 'value' contains 16 bits where
    //     bit 15 is a sign bit
    //     bit [14 .. 0] contain the absolute sample value
    //     Only the r upper bits are significant, where r is the resolution
    //     Resolution:
    //        12   -> [14 .. 3] (bitmask 0x7FF8)
    //        10   -> [14 .. 5] (bitmask 0x7FE0)
    //         9   -> [14 .. 6] (bitmask 0x7FC0)
    //         7   -> [14 .. 8] (bitmask 0x7F00)
    switch (resolution) {
	case ADC_7_BIT:
		value = (value & 0x7F00) >> 8;
		break;
	case ADC_9_BIT:
		value = (value & 0x7FC0) >> 6;
		break;
	case ADC_10_BIT:
		value = (value & 0x7FE0) >> 5;
		break;
	case ADC_12_BIT:
		value = (value & 0x7FF8) >> 3;
		break;
	default:
		value = 0;
		break;
	}
	
	return value;
}

INT16 halGetAdcValue(){
   INT16 value;
   value = ((INT16)ADCH) << 8;
   value |= (INT16)ADCL;
   return value;
}

void halAesLoadKeyOrInitVector(BYTE* pData, BOOL key){
   UINT8 i;

   // Checking whether to load a key or an initialisation vector.
   if(key){
      AES_SET_ENCR_DECR_KEY_IV(AES_LOAD_KEY);
   }
   else {
      AES_SET_ENCR_DECR_KEY_IV(AES_LOAD_IV);
   }
   // Starting loading of key or vector.
   AES_START();

   // loading the data (key or vector)
   for(i = 0; i < 16; i++){
      ENCDI = pData[i];
   }
   return;
}

void halAesEncrDecr(BYTE *pDataIn, UINT16 length, BYTE *pDataOut, BYTE *pInitVector, BOOL decr){
// Data can not be read out before AES is ready. Using a wait loop.
#define DELAY 0x0F
   UINT16 i;
   UINT8 j, k;
   BYTE mode = 0;
   UINT16 nbrOfBlocks;
   UINT16 convertedBlock;
   UINT8  delay;
   nbrOfBlocks = length / 0x10;

   if((length % 0x10) != 0){
      // length not multiplum of 16, convert one block extra with zeropadding
      nbrOfBlocks++;
   }

   // Loading the IV.
   halAesLoadKeyOrInitVector(pInitVector, FALSE);

   // Starting either encryption or decryption
   if(decr){
      AES_SET_ENCR_DECR_KEY_IV(AES_DECRYPT);
   } else {
      AES_SET_ENCR_DECR_KEY_IV(AES_ENCRYPT);
   }

   // Getting the operation mode.
   mode = ENCCS & 0x70;

   for(convertedBlock = 0; convertedBlock < nbrOfBlocks; convertedBlock++){
      // Starting the conversion.
      AES_START();

      i = convertedBlock * 16;
      // Counter, Output Feedback and Cipher Feedback operates on 4 bytes and not 16 bytes.
      if((mode == CFB) || (mode == OFB) || (mode == CTR)){
         for(j = 0; j < 4; j++){
            // Writing the input data
            // Zeropadding the remainder of the block
            for(k = 0; k < 4; k++){
               ENCDI = ((i + 4*j + k < length) ? pDataIn[i + 4*j + k] : 0x00 );
            }
            // wait for data ready
            delay = DELAY;
            while(delay--);
            // Read out data for every 4th byte
            for(k = 0; k < 4; k++){
               pDataOut[i + 4*j + k] = ENCDO;
            }
         }
      }
      else if(mode == CBC_MAC){
         // Writing the input data
         // Zeropadding the remainder of the block
         for(j = 0; j < 16; j++){
            ENCDI = ((i + j < length) ? pDataIn[i + j] : 0x00 );
         }
         // The last block of the CBC-MAC is computed by using CBC mode.
         if(convertedBlock == nbrOfBlocks - 2){
            AES_SETMODE(CBC);
            delay = DELAY;
            while(delay--);
         }
         // The CBC-MAC does not produce an output on the n-1 first blocks
         // only the last block is read out
         else if(convertedBlock == nbrOfBlocks - 1){
            // wait for data ready
            delay = DELAY;
            while(delay--);
            for(j = 0; j < 16; j++){
               pDataOut[j] = ENCDO;
            }
         }
      }
      else{
         // Writing the input data
         // Zeropadding the remainder of the block
         for(j = 0; j < 16; j++){
            ENCDI = ((i+j < length) ? pDataIn[i+j] : 0x00 );
         }
         // wait for data ready
         delay = DELAY;
         while(delay--);
         // Read out data
         for(j = 0; j < 16; j++){
            pDataOut[i+j] = ENCDO;
         }
      }
   }
}

#if 0
void halInitRandomGenerator(void)
{
   BYTE i;

   //turning on power to analog part of radio
   RFPWR = 0x04;

   //waiting for voltage regulator.
   while((RFPWR & 0x10)){}

   //Turning on 32 MHz crystal oscillator
   SET_MAIN_CLOCK_SOURCE(CRYSTAL);

   // Turning on receiver to get output from IF-ADC.
   ISRXON;
   halWait(1);

   ENABLE_RANDOM_GENERATOR();

   for(i = 0 ; i < 32 ; i++)
   {
      RNDH = ADCTSTH;
      CLOCK_RANDOM_GENERATOR();
   }

   return;
}
#endif

void halWait(BYTE wait){
   UINT32 largeWait;

   if(wait == 0)
   {return;}
   largeWait = ((UINT16) (wait << 7));
   largeWait += 114*wait;


   largeWait = (largeWait >> HAL_CLKSPD);
   while(largeWait--);

   return;
}

char getkey ()
{
   char c;
   // Turning on reception
   U0CSR |= UART_ENABLE_RECEIVE;

   while (!URX0IF);
   c = U0DBUF;
   URX0IF = FALSE;

   // Turning off reception
   U0CSR &= ~UART_ENABLE_RECEIVE;
   return c;
}

__near_func int putchar (int c)  {
   if (c == '\n')  {
      U0DBUF = 0x0d;       /* output CR  */
      while (!UTX0IF);
      UTX0IF = 0;
   }

   U0DBUF = c;
   while (!UTX0IF);
   UTX0IF = 0;
   return 0;
}