#ifndef HAL_UART1_H
#define HAL_UART1_H

#include "hal_2530.h"
typedef void (*halUART1CBack_t) (void);

extern void hal_uart1_init(halUART1CBack_t uart1CB);
extern UINT16 hal_uart1_rx_len(void);
extern UINT16 hal_uart1_read(UINT8 *buf, UINT16 len);
extern UINT16 hal_uart1_write(UINT8 *buf, UINT16 len);
extern void hal_uart1_poll(void);
#endif /* HAL_UART1_H */