
/*********************************************************************
 * INCLUDES
 */

#include "hal_uart1.h"

/*********************************************************************
 * MACROS
 */

#define HAL_UART1_RX_BUF_AVAIL() \
  (uart1Cfg.rxTail >= uart1Cfg.rxHead) ? \
  (uart1Cfg.rxTail - uart1Cfg.rxHead) : \
  (HAL_UART1_RX_MAX - uart1Cfg.rxHead + uart1Cfg.rxTail)

#define HAL_UART1_TX_BUF_AVAIL() \
  (uart1Cfg.txHead > uart1Cfg.txTail) ? \
  (uart1Cfg.txHead - uart1Cfg.txTail - 1) : \
  (HAL_UART1_TX_MAX - uart1Cfg.txTail + uart1Cfg.txHead - 1)

/*********************************************************************
 * CONSTANTS
 */

#define HAL_UART1_RX_MAX 255
#define HAL_UART1_TX_MAX 255

/*********************************************************************
 * TYPEDEFS
 */

typedef struct
{
  UINT8 rxBuf[HAL_UART1_RX_MAX];
#if HAL_UART_ISR_RX_MAX < 256
  UINT8 rxHead;
  volatile UINT8 rxTail;
#else
  UINT16 rxHead;
  volatile UINT16 rxTail;
#endif

  UINT8 txBuf[HAL_UART1_TX_MAX];
#if HAL_UART_ISR_TX_MAX < 256
  volatile UINT8 txHead;
  UINT8 txTail;
#else
  volatile UINT16 txHead;
  UINT16 txTail;
#endif

  halUART1CBack_t uart1CB;
} uartCfg_t;

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * GLOBAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static uartCfg_t uart1Cfg;

/*********************************************************************
 * LOCAL FUNCTIONS
 */

static void usart1_test_echo_cb(void);


void hal_uart1_init(halUART1CBack_t uart1CB)
{
  IO_PER_LOC_UART1_AT_PORT1_PIN4567();
  UART_SETUP(1, 
             9600, 
             FLOW_CONTROL_DISABLE
             | EIGHT_BIT_TRANSFER
             | PARITY_DISABLE
             | ONE_STOP_BITS
             | TRANSFER_MSB_LAST);
  
  USART1_FLUSH();
  
  /* ENABLE RX */
  U1CSR |= UART_ENABLE_RECEIVE;
  
  /* ENABLE RX INTERRUPT */
  INT_ENABLE(INUM_URX1, INT_ON);
  
  /* SET TX FLAG */
  INT_SETFLAG(INUM_UTX1, INT_SET);
  
  /* ATTACH CALLBACK FUNC */
  if (uart1CB) {
    uart1Cfg.uart1CB = uart1CB;
  }
  else {
    uart1Cfg.uart1CB = usart1_test_echo_cb;
  }
}

UINT16 hal_uart1_rx_len(void)
{
  return HAL_UART1_RX_BUF_AVAIL();
}

UINT16 hal_uart1_read(UINT8 *buf, UINT16 len)
{
  UINT16 cnt = 0;

  while ((uart1Cfg.rxHead != uart1Cfg.rxTail) && (cnt < len))
  {
    *buf++ = uart1Cfg.rxBuf[uart1Cfg.rxHead++];
    if (uart1Cfg.rxHead >= HAL_UART1_RX_MAX)
    {
      uart1Cfg.rxHead = 0;
    }
    cnt++;
  }

  return cnt;
}

UINT16 hal_uart1_write(UINT8 *buf, UINT16 len)
{
  UINT16 cnt;

  // Enforce all or none.
  if (HAL_UART1_TX_BUF_AVAIL() < len)
  {
    return 0;
  }

  for (cnt = 0; cnt < len; cnt++)
  {
    uart1Cfg.txBuf[uart1Cfg.txTail] = *buf++;

    if (uart1Cfg.txTail >= HAL_UART1_TX_MAX-1)
    {
      uart1Cfg.txTail = 0;
    }
    else
    {
      uart1Cfg.txTail++;
    }

    // Keep re-enabling ISR as it might be keeping up with this loop due to other ints.
    INT_ENABLE(INUM_UTX1, INT_ON);
  }

  return cnt;
}

void hal_uart1_poll(void)
{
  UINT8 st0 = ST0;
  UINT16 rx_to_read = HAL_UART1_RX_BUF_AVAIL();
  
  if (!uart1Cfg.uart1CB || rx_to_read == 0) {
    return;
  }

  uart1Cfg.uart1CB();
}

HAL_ISR_FUNCTION(halUart1RxIsr, URX1_VECTOR)
{
  UINT8 tmp = 0;
  UART1_RECEIVE(tmp);
  uart1Cfg.rxBuf[uart1Cfg.rxTail] = tmp;

  if (++uart1Cfg.rxTail >= HAL_UART1_RX_MAX)
  {
    uart1Cfg.rxTail = 0;
  }
}

HAL_ISR_FUNCTION(halUart1TxIsr, UTX1_VECTOR)
{
  if (uart1Cfg.txHead == uart1Cfg.txTail)
  {
    INT_ENABLE(INUM_UTX1, INT_OFF);
  }
  else
  {
    INT_SETFLAG(INUM_UTX1, INT_CLR);
    UART1_SEND(uart1Cfg.txBuf[uart1Cfg.txHead++]);

    if (uart1Cfg.txHead >= HAL_UART1_TX_MAX)
    {
      uart1Cfg.txHead = 0;
    }
  }
}

static void usart1_test_echo_cb(void)
{
  UINT8 temp_echo[10] = {0};
  UINT8 cnt = hal_uart1_read(temp_echo, 10);
  if (cnt > 0) {
    hal_uart1_write(temp_echo, cnt);
  }
}
