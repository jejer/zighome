/*********************************************************************
* INCLUDES
*/

#include "zh_protocol.h"
#include "hal_uart1.h"
#include <string.h>
#include "DebugTrace.h"

/*********************************************************************
* MACROS
*/

/*********************************************************************
* CONSTANTS
*/

/* Start-of-frame delimiter for UART transport */
#define ZH_UART_SOF                     0xFE

/* state values */
enum zh_state_enum {
  ZH_SOF = 0,
  ZH_LENTH,
  ZH_CMD1,
  ZH_CMD2,
  ZH_DATA,
  ZH_FCS,
};

#define ZH_RX_BUF_LEN                  254

/*********************************************************************
* TYPEDEFS
*/

typedef struct {
  enum zh_state_enum state;
  uint8 data_len;
  uint8 data_cur;
  uint8 fcs;
  uint8 app_task_id;
} zh_status_t;

/*********************************************************************
* GLOBAL VARIABLES
*/

/*********************************************************************
* GLOBAL FUNCTIONS
*/

/*********************************************************************
* LOCAL VARIABLES
*/

static zh_status_t zh_status;
static zh_proto_msg_t *p_proto_msg;

/*********************************************************************
* LOCAL FUNCTIONS
*/
static void zh_process(void);
static uint8 zh_calc_fcs(uint8 *msg_ptr, uint8 len);

void zh_uart_protocol_init(uint8 app_task_id)
{
  zh_status.state = ZH_SOF;
  zh_status.app_task_id = app_task_id;
  hal_uart1_init(zh_process);
}

/***************************************************************************************************
* @fn      zh_process(void)
*
* @brief   | SOF | Data Length  |   CMD   |   Data   |  FCS  |
*          |  1  |     1        |    2    |  0-Len   |   1   |
*
*          Parses the data then send the data to correct place
*
* @return  None
***************************************************************************************************/
static void zh_process(void)
{
  uint8  data;
  uint8  bytes_in_rx_buff;
  
  while (hal_uart1_rx_len())
  {
    hal_uart1_read(&data, 1);
    
    switch (zh_status.state)
    {
    case ZH_SOF:
      if (data == ZH_UART_SOF)
        zh_status.state = ZH_LENTH;
      break;
      
    case ZH_LENTH:
      zh_status.data_len = data;
      if (zh_status.data_len > (ZH_RX_BUF_LEN - sizeof(zh_proto_msg_t))) {
        debug_str("ZH_PROTOCOL, RX data_len too big.");
        
        /* Reset RX state */
        zh_status.data_len = 0;
        zh_status.state = ZH_SOF;
        break;
      }
      
      /* Allocate memory for the data */
      p_proto_msg = (zh_proto_msg_t *)osal_msg_allocate( sizeof ( zh_proto_msg_t ) +
                                                        zh_status.data_len );
      
      if (p_proto_msg)
      {
        /* Fill up what we can */
        p_proto_msg->hdr.event = ZH_MSG;
        p_proto_msg->msg.data_len = zh_status.data_len;
        p_proto_msg->msg.sof = ZH_UART_SOF;
        zh_status.state = ZH_CMD1;
      }
      else
      {
        debug_str("ZH_PROTOCOL, osal_msg_allocate fail.");
        zh_status.state = ZH_SOF;
        return;
      }
      break;
      
    case ZH_CMD1:
      p_proto_msg->msg.cmd1 = data;
      zh_status.state = ZH_CMD2;
      break;
      
    case ZH_CMD2:
      p_proto_msg->msg.cmd2 = data;
      /* If there is no data, skip to FCS state */
      if (zh_status.data_len)
      {
        zh_status.state = ZH_DATA;
        zh_status.data_cur = 0;
      }
      else
      {
        zh_status.state = ZH_FCS;
      }
      break;
      
    case ZH_DATA:
      
      /* Fill in the buffer the first BYTE of the data */
      p_proto_msg->msg.data[zh_status.data_cur++] = data;
      
      /* Check number of bytes left in the Rx buffer */
      bytes_in_rx_buff = hal_uart1_rx_len();
      
      /* If the remain of the data is there, read them all, otherwise, just read enough */
      if (bytes_in_rx_buff <= zh_status.data_len - zh_status.data_cur)
      {
        hal_uart1_read(&p_proto_msg->msg.data[zh_status.data_cur], bytes_in_rx_buff);
        zh_status.data_cur += bytes_in_rx_buff;
      }
      else
      {
        hal_uart1_read(&p_proto_msg->msg.data[zh_status.data_cur], zh_status.data_len - zh_status.data_cur);
        zh_status.data_cur += (zh_status.data_len - zh_status.data_cur);
      }
      
      /* If number of bytes read is equal to data length, time to move on to FCS */
      if (zh_status.data_len == zh_status.data_cur)
        zh_status.state = ZH_FCS;
      
      break;
      
    case ZH_FCS:
      
      zh_status.fcs = data;
      
      /* Make sure it's correct */
      if ((zh_calc_fcs ((uint8*)&p_proto_msg->msg, sizeof(zh_proto_t) + zh_status.data_len) == zh_status.fcs))
      {
        osal_msg_send( zh_status.app_task_id, (BYTE *)p_proto_msg );
      }
      else
      {
        debug_str("ZH_PROTOCOL, FCS doesn't match.");
        
        /* deallocate the msg */
        osal_msg_deallocate ( (uint8 *)p_proto_msg );
      }
      
      /* Reset the state, send or discard the buffers at this point */
      zh_status.state = ZH_SOF;
      
      break;
      
    default:
      break;
    }
  }
}

uint8 zh_uart_protocol_send(uint8 cmd1, uint8 cmd2, uint8 data_len, void *data)
{
  zh_proto_t *out_msg = (zh_proto_t *)osal_mem_alloc(sizeof(zh_proto_t)+1 + data_len );
  if (!out_msg) {
    debug_str("ZH_PROTOCOL, osal_mem_alloc fail.");
    return 1;
  }
  
  out_msg->sof = ZH_UART_SOF;
  out_msg->cmd1 = cmd1;
  out_msg->cmd2 = cmd2;
  out_msg->data_len = data_len;
  
  memcpy(out_msg->data, data, data_len);
  
  out_msg->data[data_len] = zh_calc_fcs((uint8 *)out_msg, sizeof(zh_proto_t) + data_len);
  
  hal_uart1_write((uint8 *)out_msg, sizeof(zh_proto_t) + data_len + 1);
  
  osal_mem_free(out_msg);
  
  return 0;
}

static uint8 zh_calc_fcs(uint8 *msg_ptr, uint8 len)
{
  uint8 x;
  uint8 xorResult;
  
  xorResult = 0;
  
  for ( x = 0; x < len; x++, msg_ptr++ )
    xorResult = xorResult ^ *msg_ptr;
  
  return ( xorResult );
}
