#ifndef ZH_PROCESS_H
#define ZH_PROCESS_H

#include "linux_list.h"
#include "zh_protocol.h"
#include "zcl.h"

enum zh_cmd1 {
  ZH_COMMON = 0,
  ZH_PLUG,
  ZH_LIGHT,
};

enum zh_cmd2 {
  ZH_COMMON_PRINT = 0,
  ZH_COMMON_DEV_LIST_REQ,
  ZH_COMMON_DEV_LIST_RSP,
  ZH_PLUG_DEV_ST_REQ,
  ZH_PLUG_DEV_ST_RSP,
  ZH_LIGHT_DEV_ST_REQ,
  ZH_LIGHT_DEV_ST_RSP,
  ZH_PLUG_ON,
  ZH_PLUG_OFF,
  ZH_LIGHT_TOGGLE,
  ZH_LIGHT_ON,
  ZH_LIGHT_OFF,
  ZH_LIGHT_COLOR,
};

enum zh_device_type {
  ZH_DEVICE_NULL = 0,
  ZH_DEVICE_PLUG,
  ZH_DEVICE_LIGHT,
};

typedef struct {
  uint8 is_on;
} zh_plug_status_t;

typedef struct {
  uint8 is_on;
  uint8 level;
  uint8 color[3];
} zh_light_status_t;

typedef struct {
  uint8             id[SADDR_EXT_LEN];
  uint8             device_type;
  uint8             shortAddr[2];
  uint8             endPoint;
} zh_dev_t;

typedef struct
{
  struct list_head head;
  uint16 Addr;  // linked target's short address
  uint8 extAddr[SADDR_EXT_LEN];
  uint16 profileID;
  uint16 deviceID;
  uint8 deviceVersion;
  uint8 EP;     // linked target's end-point
  void *pState;
} zh_device_status_t;

typedef struct {
  zh_dev_t dev_info;
} zh_msg_zl_onoff_t;

typedef struct {
  zh_dev_t dev_info;
  uint8 hue;
  uint8 sat;
} zh_msg_zl_color_t;

extern void zclZigControl_ProcessZH(zh_proto_msg_t *pMsg);

#endif