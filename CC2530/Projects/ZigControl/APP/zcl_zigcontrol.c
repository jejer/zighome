/**************************************************************************************************
Filename:       zcl_zigcontrol.c
Revised:        $Date: 2009-03-18 15:56:27 -0700 (Wed, 18 Mar 2009) $
Revision:       $Revision: 19453 $


Description:    Zigbee Cluster Library - sample device application.


Copyright 2006-2009 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED �AS IS?WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE, 
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com. 
**************************************************************************************************/

/*********************************************************************
This device will be like a Light device.  This application is not
intended to be a Light device, but will use the device description
to implement this sample code.
*********************************************************************/

/*********************************************************************
* INCLUDES
*/
#include "ZComDef.h"
#include "OSAL.h"
#include "AF.h"
#include "ZDApp.h"
#include "ZDObject.h"

#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_lighting.h"

#include "zcl_zigcontrol.h"

#include "OnBoard.h"

/* HAL */
#include "hal_lcd.h"
#include "hal_led.h"
#include "hal_key.h"
#include "zh_process.h"

#include "DebugTrace.h"
#include "MT_APP.h"
#include <stdio.h>
#include "string.h"


/*********************************************************************
* MACROS
*/

/*********************************************************************
* CONSTANTS
*/
enum {
  TCI_LISTDEVICE,
  TCI_TOGGLE,
  TCI_ON,
  TCI_OFF,
} testCmdId_t;
/*********************************************************************
* TYPEDEFS
*/
typedef struct {
  uint16            dstNwkAddr;
  uint8             dstEndpoint;
  uint16            clusterId;
  uint8             msgLen;
  uint8             cmdId;
} testMsg_t;

/*********************************************************************
* GLOBAL VARIABLES
*/
byte zclZigControl_TaskID;
LIST_HEAD(DEV_LIST);

/*********************************************************************
* GLOBAL FUNCTIONS
*/

/*********************************************************************
* LOCAL VARIABLES
*/
#define ZCLZIGCONTROL_MAX_OUTCLUSTERS      2
static cId_t zclZigControl_OutClusterList[ZCLZIGCONTROL_MAX_OUTCLUSTERS] =
{
  ZCL_CLUSTER_ID_GEN_BASIC,
  ZCL_CLUSTER_ID_GEN_ON_OFF
};

// Test Endpoint to allow SYS_APP_MSGs
static endPointDesc_t zigControl_TestEp =
{
  20,                                 // Test endpoint
  &zclZigControl_TaskID,
  (SimpleDescriptionFormat_t *)NULL,  // No Simple description for this test endpoint
  (afNetworkLatencyReq_t)0            // No Network Latency req
};


/*********************************************************************
* LOCAL FUNCTIONS
*/
static void zclZigControl_HandleKeys( byte shift, byte keys );
static void zclZigControl_BasicResetCB( void );
static void zclZigControl_ProcessZdoCB(zdoIncomingMsg_t *pMsg);
static void zclZigControl_ProcessZdoStChg(devStates_t new_state);
static void zclZigControl_Audit(void);
static void zclZigControl_Test(mtSysAppMsg_t *pMsg);

//static void zclZigControl_IdentifyCB( zclIdentify_t *pCmd );
//static void zclZigControl_IdentifyQueryRspCB( zclIdentifyQueryRsp_t *pRsp );
//static void zclZigControl_OnOffCB( uint8 cmd );
//static void zclZigControl_ProcessIdentifyTimeChange( void );

// Functions to process ZCL Foundation incoming Command/Response messages 
static void zclZigControl_ProcessIncomingMsg( zclIncomingMsg_t *msg );
#ifdef ZCL_READ
static uint8 zclZigControl_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg );
#endif
#ifdef ZCL_WRITE
static uint8 zclZigControl_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg );
#endif
static uint8 zclZigControl_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg );
#ifdef ZCL_DISCOVER
static uint8 zclZigControl_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg );
#endif

/*********************************************************************
* ZCL General Profile Callback table
*/
static zclGeneral_AppCallbacks_t zclZigControl_CmdCallbacks =
{
  zclZigControl_BasicResetCB,              // Basic Cluster Reset command
  NULL,                                     // Identify command  
  NULL,                                     // Identify Query Response command
  NULL,                                     // On/Off cluster command
  NULL,                                     // Level Control Move to Level command
  NULL,                                     // Level Control Move command
  NULL,                                     // Level Control Step command
  NULL,                                     // Group Response commands
  NULL,                                     // Scene Store Request command
  NULL,                                     // Scene Recall Request command
  NULL,                                     // Scene Response command
  NULL,                                     // Alarm (Response) command
  NULL,                                     // RSSI Location commands
  NULL,                                     // RSSI Location Response commands
};

/*********************************************************************
* @fn          zclZigControl_Init
*
* @brief       Initialization function for the zclGeneral layer.
*
* @param       none
*
* @return      none
*/
void zclZigControl_Init( byte task_id )
{
  zclZigControl_TaskID = task_id;
  
  // Set destination address to indirect
  //zclZigControl_DstAddr.addrMode = (afAddrMode_t)AddrNotPresent;
  //zclZigControl_DstAddr.endPoint = 0;
  //zclZigControl_DstAddr.addr.shortAddr = 0;
  
  // This app is part of the Home Automation Profile
  zclHA_Init( &zclZigControl_SimpleDesc );
  
  // Register the ZCL General Cluster Library callback functions
  zclGeneral_RegisterCmdCallbacks( ZIGCONTROL_ENDPOINT, &zclZigControl_CmdCallbacks );
  
  // Register the application's attribute list
  zcl_registerAttrList( ZIGCONTROL_ENDPOINT, ZIGCONTROL_MAX_ATTRIBUTES, zclZigControl_Attrs );
  
  // Register the Application to receive the unprocessed Foundation command/response messages
  zcl_registerForMsg( zclZigControl_TaskID );
  
  // Register for all key events - This app will handle all key events
  RegisterForKeys( zclZigControl_TaskID );
  
  // Register for a test endpoint
  afRegister( &zigControl_TestEp );
  
  ZDO_RegisterForZDOMsg(zclZigControl_TaskID, Device_annce);
  ZDO_RegisterForZDOMsg(zclZigControl_TaskID, Match_Desc_rsp);
  ZDO_RegisterForZDOMsg(zclZigControl_TaskID, Simple_Desc_rsp);
  
  // Start Audit
  osal_start_timerEx( zclZigControl_TaskID, ZIGCONTROL_AUDIT_TIMEOUT_EVT, 1000 );
  
  // Init zighome uart protocol
  zh_uart_protocol_init(zclZigControl_TaskID);
}

/*********************************************************************
* @fn          zclSample_event_loop
*
* @brief       Event Loop Processor for zclGeneral.
*
* @param       none
*
* @return      none
*/
uint16 zclZigControl_event_loop( uint8 task_id, uint16 events )
{
  (void)task_id;  // Intentionally unreferenced parameter
  osal_event_hdr_t *pMsg;
  
  if ( events & SYS_EVENT_MSG )
  {
    while ( (pMsg = (osal_event_hdr_t *)osal_msg_receive( zclZigControl_TaskID )) )
    {
      switch ( pMsg->event )
      {
      case ZDO_CB_MSG:
        zclZigControl_ProcessZdoCB((zdoIncomingMsg_t *)pMsg);
        break;
        
      case ZDO_STATE_CHANGE:
        zclZigControl_ProcessZdoStChg((devStates_t)pMsg->status);
        break; 
        
      case ZCL_INCOMING_MSG:
        // Incoming ZCL Foundation command/response messages
        zclZigControl_ProcessIncomingMsg( (zclIncomingMsg_t *)pMsg );
        break;
        
      case KEY_CHANGE:
        zclZigControl_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
        break;
        
      case MT_SYS_APP_MSG:
        zclZigControl_Test((mtSysAppMsg_t *)pMsg);
        break;
        
      case ZH_MSG:
        zclZigControl_ProcessZH((zh_proto_msg_t *)pMsg);
        break;
        
      default:
        break;
      }
      
      // Release the memory
      osal_msg_deallocate( (uint8 *)pMsg );
    }
    
    // return unprocessed events
    return (events ^ SYS_EVENT_MSG);
  }
  
  if ( events & ZIGCONTROL_AUDIT_TIMEOUT_EVT )
  {
    zclZigControl_Audit();
    
    // Restart Audit Again
    osal_start_timerEx( zclZigControl_TaskID, ZIGCONTROL_AUDIT_TIMEOUT_EVT, 1000 );
    
    return ( events ^ ZIGCONTROL_AUDIT_TIMEOUT_EVT );
  }
  
  // Discard unknown events
  return 0;
}

/*********************************************************************
* @fn      zclZigControl_HandleKeys
*
* @brief   Handles all key events for this device.
*
* @param   shift - true if in shift/alt.
* @param   keys - bit field for key events. Valid entries:
*                 HAL_KEY_SW_4
*                 HAL_KEY_SW_3
*                 HAL_KEY_SW_2
*                 HAL_KEY_SW_1
*
* @return  none
*/
static void zclZigControl_HandleKeys( byte shift, byte keys )
{
  //zAddrType_t dstAddr;
  
  (void)shift;  // Intentionally unreferenced parameter
  
  if ( keys & HAL_KEY_SW_1 )
  {
    debug_str("Reset Key Pushed!");
    
    NLME_InitNV();
    NLME_SetDefaultNV();
    
    SystemResetSoft();
  }
}

/*********************************************************************
* @fn      zclZigControl_BasicResetCB
*
* @brief   Callback from the ZCL General Cluster Library
*          to set all the Basic Cluster attributes to default values.
*
* @param   none
*
* @return  none
*/
static void zclZigControl_BasicResetCB( void )
{
  // Reset all attributes to default values
}

/****************************************************************************** 
* 
*  Functions for processing ZCL Foundation incoming Command/Response messages
*
*****************************************************************************/

/*********************************************************************
* @fn      zclZigControl_ProcessIncomingMsg
*
* @brief   Process ZCL Foundation incoming message
*
* @param   pInMsg - pointer to the received message
*
* @return  none
*/
static void zclZigControl_ProcessIncomingMsg( zclIncomingMsg_t *pInMsg)
{
  switch ( pInMsg->zclHdr.commandID )
  {
#ifdef ZCL_READ
  case ZCL_CMD_READ_RSP:
    zclZigControl_ProcessInReadRspCmd( pInMsg );
    break;
#endif
#ifdef ZCL_WRITE    
  case ZCL_CMD_WRITE_RSP:
    zclZigControl_ProcessInWriteRspCmd( pInMsg );
    break;
#endif
#ifdef ZCL_REPORT
    // See ZCL Test Applicaiton (zcl_testapp.c) for sample code on Attribute Reporting
  case ZCL_CMD_CONFIG_REPORT:
    //zclZigControl_ProcessInConfigReportCmd( pInMsg );
    break;
    
  case ZCL_CMD_CONFIG_REPORT_RSP:
    //zclZigControl_ProcessInConfigReportRspCmd( pInMsg );
    break;
    
  case ZCL_CMD_READ_REPORT_CFG:
    //zclZigControl_ProcessInReadReportCfgCmd( pInMsg );
    break;
    
  case ZCL_CMD_READ_REPORT_CFG_RSP:
    //zclZigControl_ProcessInReadReportCfgRspCmd( pInMsg );
    break;
    
  case ZCL_CMD_REPORT:
    //zclZigControl_ProcessInReportCmd( pInMsg );
    break;
#endif   
  case ZCL_CMD_DEFAULT_RSP:
    zclZigControl_ProcessInDefaultRspCmd( pInMsg );
    break;
#ifdef ZCL_DISCOVER     
  case ZCL_CMD_DISCOVER_RSP:
    zclZigControl_ProcessInDiscRspCmd( pInMsg );
    break;
#endif  
  default:
    break;
  }
  
  if ( pInMsg->attrCmd )
    osal_mem_free( pInMsg->attrCmd );
}

#ifdef ZCL_READ
/*********************************************************************
* @fn      zclZigControl_ProcessInReadRspCmd
*
* @brief   Process the "Profile" Read Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclZigControl_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg )
{
  zclReadRspCmd_t *readRspCmd;
  uint8 i;
  
  readRspCmd = (zclReadRspCmd_t *)pInMsg->attrCmd;
  for (i = 0; i < readRspCmd->numAttr; i++)
  {
    // Notify the originator of the results of the original read attributes 
    // attempt and, for each successfull request, the value of the requested 
    // attribute
  }
  
  return TRUE; 
}
#endif // ZCL_READ

#ifdef ZCL_WRITE
/*********************************************************************
* @fn      zclZigControl_ProcessInWriteRspCmd
*
* @brief   Process the "Profile" Write Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclZigControl_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg )
{
  zclWriteRspCmd_t *writeRspCmd;
  uint8 i;
  
  writeRspCmd = (zclWriteRspCmd_t *)pInMsg->attrCmd;
  for (i = 0; i < writeRspCmd->numAttr; i++)
  {
    // Notify the device of the results of the its original write attributes
    // command.
  }
  
  return TRUE; 
}
#endif // ZCL_WRITE

/*********************************************************************
* @fn      zclZigControl_ProcessInDefaultRspCmd
*
* @brief   Process the "Profile" Default Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclZigControl_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg )
{
  // zclDefaultRspCmd_t *defaultRspCmd = (zclDefaultRspCmd_t *)pInMsg->attrCmd;
  
  // Device is notified of the Default Response command.
  (void)pInMsg;
  
  return TRUE; 
}

#ifdef ZCL_DISCOVER
/*********************************************************************
* @fn      zclZigControl_ProcessInDiscRspCmd
*
* @brief   Process the "Profile" Discover Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclZigControl_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg )
{
  zclDiscoverRspCmd_t *discoverRspCmd;
  uint8 i;
  
  discoverRspCmd = (zclDiscoverRspCmd_t *)pInMsg->attrCmd;
  for ( i = 0; i < discoverRspCmd->numAttr; i++ )
  {
    // Device is notified of the result of its attribute discovery command.
  }
  
  return TRUE;
}
#endif // ZCL_DISCOVER

void zclZigControl_ProcessZdoCB(zdoIncomingMsg_t *pMsg)
{
  ZDO_DeviceAnnce_t devAnnce;
  struct list_head *pos;
  zh_device_status_t *p_dev_st = NULL;
  ZDO_SimpleDescRsp_t simpleDescRsp;
  ZDO_ActiveEndpointRsp_t *pAeRsp;
  zAddrType_t dstAddr;
  
  switch (pMsg->clusterID) {
  case Device_annce:
    // Device Announce
    // New device may have conflict with existing devices
    ZDO_ParseDeviceAnnce(pMsg, &devAnnce);
    
    // Check if device already in list
    list_for_each(pos, &DEV_LIST) {
      p_dev_st = (zh_device_status_t *)pos;
      if (osal_ExtAddrEqual(devAnnce.extAddr, p_dev_st->extAddr)) {
        break;
      }
      p_dev_st = NULL;
    }
    
    // New Devicve
    if (!p_dev_st) {
      zh_device_status_t *p_dev_st = (zh_device_status_t *)osal_mem_alloc(sizeof(zh_device_status_t));
      if (p_dev_st) {
        memset(p_dev_st, 0, sizeof(zh_device_status_t));
        p_dev_st->Addr = devAnnce.nwkAddr;
        osal_cpyExtAddr(p_dev_st->extAddr, devAnnce.extAddr);
        list_add(&(p_dev_st->head), &DEV_LIST);
      }
    }
    else {
      list_del(&(p_dev_st->head));
      if (p_dev_st->pState) {
        osal_mem_free(p_dev_st->pState);
      }
      memset(p_dev_st, 0, sizeof(zh_device_status_t));
      p_dev_st->Addr = devAnnce.nwkAddr;
      osal_cpyExtAddr(p_dev_st->extAddr, devAnnce.extAddr);
      list_add(&(p_dev_st->head), &DEV_LIST);
    }
    break;
    
  case Match_Desc_rsp:
    pAeRsp = ZDO_ParseEPListRsp( pMsg );
    dstAddr.addr.shortAddr = pAeRsp->nwkAddr;
    dstAddr.addrMode = Addr16Bit;
    for (int i = 0; i < pAeRsp->cnt; ++i) {
      ZDP_SimpleDescReq( &dstAddr, pAeRsp->nwkAddr,
                        pAeRsp->epList[i], FALSE);
    }
    osal_mem_free(pAeRsp);
    break;
    
  case Simple_Desc_rsp:
    ZDO_ParseSimpleDescRsp(pMsg, &simpleDescRsp);
    list_for_each(pos, &DEV_LIST) {
      p_dev_st = (zh_device_status_t *)pos;
      if (simpleDescRsp.nwkAddr == p_dev_st->Addr) {
        break;
      }
      p_dev_st = NULL;
    } 
    if (p_dev_st) {
      if (simpleDescRsp.simpleDesc.AppDeviceId == ZCL_HA_DEVICEID_COLORED_DIMMABLE_LIGHT
          || simpleDescRsp.simpleDesc.AppDeviceId == ZCL_HA_DEVICEID_ON_OFF_OUTPUT) {
            p_dev_st->profileID = simpleDescRsp.simpleDesc.AppProfId;
            p_dev_st->deviceID = simpleDescRsp.simpleDesc.AppDeviceId;
            p_dev_st->deviceVersion = simpleDescRsp.simpleDesc.AppDevVer;
            p_dev_st->EP = simpleDescRsp.simpleDesc.EndPoint;
            
            //Check and add to NV
          }
    }
    osal_mem_free(simpleDescRsp.simpleDesc.pAppInClusterList);
    osal_mem_free(simpleDescRsp.simpleDesc.pAppOutClusterList);
    break;
    
  default:
    break;
  }
}

void zclZigControl_ProcessZdoStChg(devStates_t new_state)
{
  switch (new_state) {
  case DEV_COORD_STARTING:
    // Device starting network
    HalLedSet(HAL_LED_1, HAL_LED_MODE_BLINK);
    break;
  case DEV_ZB_COORD:
    // Device is in coordinator mode
    HalLedSet(HAL_LED_1, HAL_LED_MODE_ON);
    break;
  default:
    // Something wrong
    HalLedSet(HAL_LED_1, HAL_LED_MODE_OFF);
    break;
  }
}

void zclZigControl_Audit(void)
{
  struct list_head *pos;
  zh_device_status_t *p_dev_st;
  
  zAddrType_t dstAddr;
  
  //Check new device
  list_for_each(pos, &DEV_LIST) {
    p_dev_st = (zh_device_status_t *)pos;
    if (p_dev_st->profileID == 0) {
      dstAddr.addr.shortAddr = p_dev_st->Addr;
      dstAddr.addrMode = Addr16Bit;
      ZDP_MatchDescReq( &dstAddr, p_dev_st->Addr,
                       ZCL_HA_PROFILE_ID,
                       ZCLZIGCONTROL_MAX_OUTCLUSTERS, zclZigControl_OutClusterList,
                       0, NULL, FALSE );    
    }
  }
  
  //Kick off Non-HA devices
  
  //Check light state
  
  //Check plug state
  
  //Check NV and device consistancy
}

void zclZigControl_Test(mtSysAppMsg_t *pMsg)
{
  testMsg_t *pTestMsg;
  char debugLine[128];
  struct list_head *pos;
  zh_device_status_t *p_dev_st;
  afAddrType_t dstAddr;
  
  pTestMsg = (testMsg_t *)pMsg->appData;
  switch (pTestMsg->cmdId) {
  case TCI_LISTDEVICE:
    debug_str("IEEEaddress NWKaddr EndPoint DEV_ID");
    list_for_each(pos, &DEV_LIST) {
      p_dev_st = (zh_device_status_t *)pos;
      sprintf(debugLine, "%x%x%x%x%x%x%x%x %x %x %x%x",
              p_dev_st->extAddr[0], p_dev_st->extAddr[1], p_dev_st->extAddr[2], p_dev_st->extAddr[3], 
              p_dev_st->extAddr[4], p_dev_st->extAddr[5], p_dev_st->extAddr[6], p_dev_st->extAddr[7], 
              p_dev_st->Addr, p_dev_st->EP, HI_UINT16(p_dev_st->deviceID), LO_UINT16(p_dev_st->deviceID));
      debug_str((unsigned char *)debugLine);
    }
    debug_str("-----END-----");
    break;
  case TCI_TOGGLE:
    dstAddr.addr.shortAddr = pTestMsg->dstNwkAddr;
    dstAddr.addrMode = (afAddrMode_t)Addr16Bit;
    dstAddr.endPoint = pTestMsg->dstEndpoint;
    zclGeneral_SendOnOff_CmdToggle(ZIGCONTROL_ENDPOINT, &dstAddr, false, 0);
    break;
  default:
    break;
  }
}




/****************************************************************************
****************************************************************************/


