#ifndef ZH_PROTOCOL_H
#define ZH_PROTOCOL_H

#include "OSAL.h"

#define ZH_MSG      0x07

typedef struct
{
  uint8             sof;
  uint8             data_len;
  uint8             cmd1;
  uint8             cmd2;
  uint8             data[0];
} zh_proto_t;

typedef struct
{
  osal_event_hdr_t  hdr;
  zh_proto_t        msg;
} zh_proto_msg_t;

extern void zh_uart_protocol_init(uint8 app_task_id);
extern uint8 zh_uart_protocol_send(uint8 cmd1, uint8 cmd2, uint8 data_len, void *data);

#endif