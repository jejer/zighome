/*********************************************************************
* INCLUDES
*/

#include "zh_process.h"
#include <stdio.h>  // for printf
#include "DebugTrace.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_lighting.h"

#include "zcl_zigcontrol.h"


/*********************************************************************
* MACROS
*/

/*********************************************************************
* CONSTANTS
*/

/*********************************************************************
* TYPEDEFS
*/

/*********************************************************************
* GLOBAL VARIABLES
*/

/*********************************************************************
* GLOBAL FUNCTIONS
*/

/*********************************************************************
* LOCAL VARIABLES
*/

/*********************************************************************
* LOCAL FUNCTIONS
*/

void zclZigControl_ProcessZH(zh_proto_msg_t *pMsg)
{
  zh_device_status_t *p_dev_st = NULL;
  uint8 count = 0;
  uint8 *pData = NULL;
  zh_dev_t *pMsg_dev = NULL;
  afAddrType_t dstAddr;
  static char debugLine[32];
  sprintf(debugLine, "CMD1(%02x) CMD2(%02x) DATA_LEN(%d)\r\n", 
          pMsg->msg.cmd1, pMsg->msg.cmd2, pMsg->msg.data_len);
  debug_str((unsigned char *)debugLine);
  
  //zh_uart_protocol_send(pMsg->msg.cmd1, pMsg->msg.cmd2, pMsg->msg.data_len, pMsg->msg.data);
  switch (pMsg->msg.cmd1) {
  case ZH_COMMON:
    switch (pMsg->msg.cmd2) {
    case ZH_COMMON_PRINT:
      break;
    case ZH_COMMON_DEV_LIST_REQ:
      if (list_empty(&DEV_LIST)) {
        zh_uart_protocol_send(ZH_COMMON, ZH_COMMON_DEV_LIST_RSP, 0, NULL);
        break;
      }
      list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
        ++count;
      }
      
      pData = (uint8 *)osal_mem_alloc(sizeof(zh_dev_t)*count);
      if (!pData) {
        debug_str("zclZigControl_ProtoZH osal_msg_allocate fail.");
        return;
      }
      osal_memset(pData, 0, sizeof(zh_dev_t)*count);
      
      count = 0;
      list_for_each_entry_type(p_dev_st, zh_device_status_t, &DEV_LIST, head) {
        pMsg_dev = (zh_dev_t *)(pData + sizeof(zh_dev_t)*count);
        
        osal_cpyExtAddr(pMsg_dev->id, p_dev_st->extAddr);
        pMsg_dev->shortAddr[0] = p_dev_st->Addr >> 8;
        pMsg_dev->shortAddr[1] = p_dev_st->Addr & 0xFF;
        pMsg_dev->endPoint = p_dev_st->EP;
        
        switch (p_dev_st->deviceID) {
        case ZCL_HA_DEVICEID_COLORED_DIMMABLE_LIGHT:
          pMsg_dev->device_type = ZH_DEVICE_LIGHT;
          break;
        case ZCL_HA_DEVICEID_ON_OFF_OUTPUT:
          pMsg_dev->device_type = ZH_DEVICE_PLUG;
          break;
          default:
            pMsg_dev->device_type = ZH_DEVICE_NULL;
            break;
        }
        ++count;
      }
      zh_uart_protocol_send(ZH_COMMON, ZH_COMMON_DEV_LIST_RSP, sizeof(zh_dev_t)*count, pData);
      osal_mem_free(pData);
      break;
    case ZH_COMMON_DEV_LIST_RSP:
      break;
    default:
      break;
    }
    break;
    
  case ZH_PLUG:
    switch (pMsg->msg.cmd2) {
    case ZH_PLUG_ON:
      break;
    case ZH_PLUG_OFF:
      break;
    default:
      break;
    }
    break;
    
  case ZH_LIGHT:
    pMsg_dev = (zh_dev_t*)pMsg->msg.data;
    dstAddr.addr.shortAddr = (pMsg_dev->shortAddr[0] << 8) + pMsg_dev->shortAddr[1];
    dstAddr.addrMode = (afAddrMode_t)Addr16Bit;
    dstAddr.endPoint = pMsg_dev->endPoint;
    
    switch (pMsg->msg.cmd2) {
    case ZH_LIGHT_TOGGLE:
      zclGeneral_SendOnOff_CmdToggle(ZIGCONTROL_ENDPOINT, &dstAddr, false, 0);
      break;
    case ZH_LIGHT_ON:
      zclGeneral_SendOnOff_CmdOn(ZIGCONTROL_ENDPOINT, &dstAddr, false, 0);
      break;
    case ZH_LIGHT_OFF:
      zclGeneral_SendOnOff_CmdOff(ZIGCONTROL_ENDPOINT, &dstAddr, false, 0);
      break;
    case ZH_LIGHT_COLOR:
      zclLighting_ColorControl_Send_MoveToHueAndSaturationCmd(ZIGCONTROL_ENDPOINT, 
                                                              &dstAddr,
                                                              *(pMsg->msg.data + sizeof(zh_dev_t)),      /* Hue */
                                                              *(pMsg->msg.data + sizeof(zh_dev_t) + 1),  /* Saturation */
                                                              10, false, 0);
      break;
    default:
      break;
    }
    break;
    
  default:
    break;
  }
}