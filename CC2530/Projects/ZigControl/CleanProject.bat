@echo off
pushd .
cd %~dp0
echo CMD PATH: %cd%
rmdir /s /q Output
cd Project
del /s *.dep
del /s *.sfr
rmdir /s /q settings
popd
